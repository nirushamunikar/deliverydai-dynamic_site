@extends('layouts.app')
@section('title','Food')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add Food</h2>
            <div class="col-md-9">
                <form action="{{route('food.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Food Category</label>
                        <select name="food_category_id" id="" class="form-control">
                            @foreach($foodcats as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Food SubCategory</label>
                        <select name="food_subcategory_id" id="" class="form-control">
                            @foreach($foodsubcats as $cats)
                                <option value="{{$cats->id}}">{{$cats->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Food Type</label>
                        <select name="food_type_id" id="" class="form-control">
                            @foreach($foodtype as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea type="text" class="form-control ckeditor" name="description" placeholder="description"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Favourate Count</label>
                        <input type="text" name="fav_count"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add Food</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


