@extends('layouts.app')
@section('title','Food')
@section('breadcrumb')
    <h1>
        Food Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Food <span class="pull-right"><a href="{{route('food.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Food Category</th>
                        <th>Food SubCategory</th>
                        <th>Food Type</th>
                        <th>Description</th>
                        <th>Favourate Count</th>
                        <th>Recommended</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->food_category_id}}</td>
                            <td>{{$d->food_subcategory_id}}</td>
                            <td>{{$d->food_type_id}}</td>
                            <td>{{$d->description}}</td>
                            <td>{{$d->fav_count}}</td>
                            <td>{{$d->recommended}}</td>
                            <td><a href="{{route('food.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('food.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
