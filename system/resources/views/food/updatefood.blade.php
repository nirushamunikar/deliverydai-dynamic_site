@extends('layouts.app')
@section('title','Food')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit Food</h2>
            <div class="col-md-9">

                <form action="{{route('food.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="name" value="{{$data->name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Food Category</label>
                        <select name="food_category_id" id="" class="form-control">
                            @foreach($foodcats as $cat)
                                <option value="{{$cat->id}}" {{($data->food_category_id == $cat->id) ? 'selected':''}} >{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Food SubCategory</label>
                        <select name="food_subcategory_id" id="" class="form-control">
                            @foreach($foodsubcats as $cats)
                                <option value="{{$cats->id}}" {{($data->food_subcategory_id == $cats->id) ? 'selected':''}} >{{$cats->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Food Type</label>
                        <select name="food_type_id" id="" class="form-control">
                            @foreach($foodtype as $type)
                                <option value="{{$type->id}}" {{($data->food_type_id == $type->id) ? 'selected':''}} >{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Description</label>
                        <textarea type="text" class="form-control ckeditor" name="description">{{$data->description}}</textarea>
                    </div>

                    <div class="form-group">
                        <label >Favourate Count</label>
                        <input type="text" name="fav_count" value="{{$data->fav_count}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="0"{{($data['recommended'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['recommended'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update Food</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection