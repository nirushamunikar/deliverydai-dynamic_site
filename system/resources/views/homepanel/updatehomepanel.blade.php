@extends('layouts.app');
@section('title','HomePanel')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit Homepanel</h2>
            <div class="col-md-9">

                <form action="{{route('homepanel.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label>Logo</label><br/>
                        <img src="{{asset('assets/images/homepanel/logo/'.$data->logo)}}">
                        <input type="file" name="logo" >
                    </div>

                    <div class="form-group">
                        <label>Background Image</label><br/>
                        <img src="{{asset('assets/images/homepanel/background/'.$data->bgimage)}}" height="150px"; width="300px";>
                        <input type="file" name="bgimage" >
                    </div>

                    <div class="form-group">
                        <label >Caption</label>
                        <input type="text" name="caption" value="{{$data->caption}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Flip Text</label>
                        <input type="text" name="videourl" value="{{$data->videourl}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Twitter Link</label>
                        <input type="text" name="twitterlink" value="{{$data->twitterlink}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>FaceBook Link</label>
                        <input type="text" name="fblink" value="{{$data->fblink}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Instagram Link</label>
                        <input type="text" name="instalink" value="{{$data->instalink}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Google Link</label>
                        <input type="text" name="googlelink" value="{{$data->googlelink}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Status</label>
                        <select name="status" class="form-control">
                            <option value="0"{{($data['status'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['status'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update Homepanel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

