@extends('layouts.app')
@section('title','Home Panel')
@section('breadcrumb')
    <h1>
        HomePanel Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>HomePanel Table<span class="pull-right"><a href="{{route('homepanel.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.No</th>
                        <th>Logo</th>
                        <th>Background Image</th>
                        <th>Caption</th>
                        <th>Vide Ourl</th>
                        <th>Twitter Link</th>
                        <th>Facebook Link</th>
                        <th>Instagram Link</th>
                        <th>Google Link</th>
                        <th>status</th>
                        <th>Option</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>
                            <td>{{$i}}</td>
                            <td><img src="{{asset('assets/images/homepanel/logo/'.$d->logo)}}" class="img-thumbnail" /></td>
                            <td><img src="{{asset('assets/images/homepanel/background/'.$d->bgimage)}}" class="img-thumbnail" /></td>
                            <td>{{$d->caption}}</td>
                            <td>{{$d->videourl}}</td>
                            <td>{{$d->twitterlink}}</td>
                            <td>{{$d->fblink}}</td>
                            <td>{{$d->instalink}}</td>
                            <td>{{$d->googlelink}}</td>
                            <td>{{$d->status}}</td>
                            <td><a href="{{route('homepanel.edit',$d->id)}}">Edit</a>||<a href="{{route('homepanel.delete',$d->id)}}">Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
