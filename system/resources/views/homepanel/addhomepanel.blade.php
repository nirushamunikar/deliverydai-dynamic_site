@extends('layouts.app')
@section('title','HomePanel')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Add HomePanel</h1>
            <div class="col-md-9">
                <form action="{{route('homepanel.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group ">
                        <label>Logo</label>
                        <input type="file" name="logo" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Caption</label>
                        <input type="text" name="caption" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Background Image</label>
                        <input type="file" name="bgimage" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Video Url</label>
                        <input type="text" name="videourl" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Twitter Link</label>
                        <input type="text" name="twitterlink" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>FaceBook Link</label>
                        <input type="text" name="fblink" class="form-control ">
                    </div>


                    <div class="form-group ">
                        <label>Instagram Link</label>
                        <input type="text" name="instalink" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Google Link</label>
                        <input type="text" name="googlelink" class="form-control ">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add HomePanel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection