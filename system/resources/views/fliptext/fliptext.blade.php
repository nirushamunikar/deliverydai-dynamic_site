@extends('layouts.app')
@section('title','FlipText')
@section('breadcrumb')
    <h1>
        FlipText Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="container">
                    <h2>Flip Text
                    <!-- Trigger the modal with a button -->
                    <span class="pull-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fliptext"><i class="fa fa-plus"></i> Add New</button>
                    </span>
                    </h2>
                    <!-- Modal -->

                    <form action="{{route('fliptext.save')}}" method="post" enctype="multipart/form-data" class="modal fade" id="fliptext" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                {{csrf_field()}}

                                <div class="form-control modal-title">
                                    <label> Add FlipText</label>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <div class="form-group modal-body" >
                                    <label>Text</label>
                                    <input type="text" name="text"class="form-control">

                                    <br>

                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="1">Publish</option>
                                        <option value="0">Unpublished</option>
                                    </select>

                                </div>

                                <div class="form-group modal-footer">
                                    <button type="submit" class="btn btn-success" value="save">Add FlipText</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>

                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Text</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->text}}</td>
                            <td>{{$d->status}}</td>
                            <td><a href="{{route('fliptext.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('fliptext.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
