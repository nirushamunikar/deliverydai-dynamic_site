@extends('layouts.app')
@section('title','FlipText')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add FlipText</h2>
            <div class="col-md-9">
                <form action="{{route('fliptext.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Text</label>
                        <input type="text" name="text"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add FlipText</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


