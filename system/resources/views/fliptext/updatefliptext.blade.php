@extends('layouts.app');
@section('title','FlipText')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit FlipText</h2>
            <div class="col-md-9">

                <form action="{{route('fliptext.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Text</label>
                        <input type="text" name="text" value="{{$data->text}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Status</label>
                        <select name="status" class="form-control">
                            <option value="0"{{($data['status'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['status'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update FlipText</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection