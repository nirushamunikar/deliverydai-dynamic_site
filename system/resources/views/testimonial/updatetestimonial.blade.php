@extends('layouts.app');
@section('title','Testimonial')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit Testimonial</h2>
            <div class="col-md-9">

                <form action="{{route('testimonial.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="name" value="{{$data->name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Review</label>
                        <textarea type="text" class="form-control ckeditor" name="review">{{$data->review}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>photo</label><br/>
                        <img src="{{asset('assets/images/testimonial/'.$data->photo)}}">
                        <input type="file" name="photo" >
                    </div>

                    <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" name="company_name" value="{{$data->company_name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Position</label>
                        <input type="text" name="position" value="{{$data->position}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Status</label>
                        <select name="status" class="form-control">
                            <option value="0"{{($data['status'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['status'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update Homepanel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

