@extends('layouts.app')
@section('title','Testimonials')
@section('breadcrumb')
    <h1>
        Testimonials
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Testimonials<span class="pull-right"><a href="{{route('testimonial.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Review</th>
                        <th>Photo</th>
                        <th>Company Name</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->review}}</td>
                            <td><img src="{{asset('assets/images/testimonial/'.$d->photo)}}" class="img-thumbnail" style="height: 100px; width: 150px;" /></td>
                            <td>{{$d->company_name}}</td>
                            <td>{{$d->position}}</td>
                            <td>{{$d->status}}</td>
                            <td><a href="{{route('testimonial.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{route('testimonial.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
