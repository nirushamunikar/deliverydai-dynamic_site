@extends('layouts.app')
@section('title','Testimonial')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Add Testimonial</h1>
            <div class="col-md-9">
                <form action="{{route('testimonial.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Review</label>
                        <textarea type="text" class="form-control ckeditor" name="review" placeholder="Write A Review!!!"></textarea>

                    </div>

                    <div class="form-group ">
                        <label>Photo</label>
                        <input type="file" name="photo" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Company Name</label>
                        <input type="text" name="company_name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Position</label>
                        <input type="text" name="position" class="form-control ">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add Testimonial</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection