@extends('layouts.app');
@section('title','FoodFavourate')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit FoodFavourate</h2>
            <div class="col-md-9">

                <form action="{{route('foodfavourate.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Food Name</label>
                        <select name="food_id" id="" class="form-control">
                            @foreach($foods as $food)
                                <option value="{{$food->id}}" {{($data->food_id == $food->id) ? 'selected':''}} >{{$food->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Customer Name</label>
                        <input type="text" name="customer_id" value="{{$data->customer_id}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Status</label>
                        <select name="status" class="form-control">
                            <option value="0"{{($data['status'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['status'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update FoodFavourate</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection