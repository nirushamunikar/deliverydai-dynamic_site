@extends('layouts.app')
@section('title','FoodFavourate')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add FoodFavourate</h2>
            <div class="col-md-9">
                <form action="{{route('foodfavourate.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Food Name</label>
                        <select name="food_id" id="" class="form-control">
                            @foreach($foods as $food)
                                <option value="{{$food->id}}">{{$food->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="customer_id"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add FoodFavourate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


