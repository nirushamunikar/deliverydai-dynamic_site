@extends('layouts.app')
@section('title','User')
@section('breadcrumb')
    <h1>
        User Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>User <span class="pull-right"><a href="{{route('user.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Social User Id</th>
                        <th>Social Access Token</th>
                        <th>Image</th>
                        <th>Last Login</th>
                        <th>Permission</th>
                        <th>Ip Address</th>
                        <th>Role</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->first_name}}</td>
                            <td>{{$d->last_name}}</td>
                            <td>{{$d->address}}</td>
                            <td>{{$d->email}}</td>
                            <td>{{$d->password}}</td>
                            <td>{{$d->social_user_id}}</td>
                            <td>{{$d->social_access_token}}</td>
                            <td>{{$d->image}}</td>
                            <td>{{$d->last_login}}</td>
                            <td>{{$d->permissions}}</td>
                            <td>{{$d->ip_address}}</td>
                            <td>{{$d->role}}</td>
                            <td><a href="{{route('user.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('user.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
