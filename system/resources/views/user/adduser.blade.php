@extends('layouts.app')
@section('title','User')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Add User</h1>
            <div class="col-md-9">
                <form action="{{route('user.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Address</label>
                        <input type="text" name="address" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Password</label>
                        <input type="text" name="password" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Social User Id</label>
                        <input type="text" name="social_user_id" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Social Access Token</label>
                        <input type="text" name="social_access_token" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Image</label>
                        <input type="file" name="image" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Last Login</label>
                        <input type="text" name="last_login" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Permission</label>
                        <input type="text" name="permission" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Ip Address</label>
                        <input type="text" name="ip_address" class="form-control ">
                    </div>

                    <div class="form-group">
                        <label>Role</label>
                        <select name="role" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection