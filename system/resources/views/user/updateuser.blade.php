@extends('layouts.app');
@section('title','User')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit User</h2>
            <div class="col-md-9">

                <form action="{{route('user.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="name" value="{{$data->name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >First Name</label>
                        <input type="text" name="first_name" value="{{$data->first_name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Last Name</label>
                        <input type="text" name="last_name" value="{{$data->last_name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Address</label>
                        <input type="text" name="address" value="{{$data->address}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Email</label>
                        <input type="email" name="email" value="{{$data->email}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Password</label>
                        <input type="text" name="password" value="{{$data->password}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Social User Id</label>
                        <input type="text" name="social_user_id" value="{{$data->social_user_id}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Social Access Token</label>
                        <input type="text" name="social_access_token" value="{{$data->social_access_token}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Image</label><br/>
                        <img src="{{asset('assets/images/user/'.$data->image)}}">
                        <input type="file" name="image" >
                    </div>

                    <div class="form-group">
                        <label>Last Login</label>
                        <input type="text" name="last_login" value="{{$data->last_login}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Permission</label>
                        <input type="text" name="permission" value="{{$data->permission}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Ip Address</label>
                        <input type="text" name="ip_address" value="{{$data->ip_address}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Role</label>
                        <select name="role" class="form-control">
                            <option value="0"{{($data['role'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['role'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update User</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

