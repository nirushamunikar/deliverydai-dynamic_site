@extends('layouts.app');
@section('title','RestaurantInfo')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit RestaurantInfo</h2>
            <div class="col-md-9">

                <form action="{{route('restaurantinfo.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="name" value="{{$data->name}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Address</label>
                        <input id="searchTextField" type="text" name="address" class="form-control" autocomplete="on" runat="server" value="{{$data->address}}" >
                    </div>

                    <input type="hidden" name="latitude" id="latitude" value="{{$data->latitude}}" class="form-control">

                    <input type="hidden" name="longitude" id="longitude" value="{{$data->longitude}}" class="form-control">

                    <div class="form-group">
                        <label>Logo</label><br/>
                        <img src="{{asset('assets/images/restaurant/logo/'.$data->logo)}}">
                        <input type="file" name="logo" >
                    </div>

                    <div class="form-group">
                        <label>BackGround Image</label><br/>
                        <img src="{{asset('assets/images/restaurant/background/'.$data->bgimage)}}" height="400px"; width="900px";>
                        <input type="file" name="bgimage" >
                    </div>

                    <div class="form-group">
                        <label >Restaurant Type</label>
                        <input type="text" name="restaurant_type" value="{{$data->restaurant_type}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Custom Field Name</label>
                        <input type="text" name="custom_field_name" value="{{$data->custom_field_name}}" class="form-control">
                    </div>


                    <div class="form-group">
                        <label>Custom Field Value</label>
                        <input type="text" name="custom_field_value" value="{{$data->custom_field_value}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Deivery Time</label>
                        <input type="text" name="delivery_time" value="{{$data->delivery_time}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Service Charge</label>
                        <input type="text" name="service_charge" value="{{$data->service_charge}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Vat</label>
                        <input type="text" name="vat" value="{{$data->vat}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Restaurant Category</label>
                        <?php
                        $dat = json_decode($data->restaurant_category);

                        ?>
                        <select class="form-control selectpicker" multiple name="restaurant_category[]">

                            <option value="pickofthepack" <?php echo (in_array('pickofthepack',$dat)) ?  'selected':''; ?>>Pick of the Pack</option>
                            <option value="vegoption" <?php echo (in_array('vegoption',$dat)) ?  'selected':''; ?>>Veg Option</option>
                            <option value="halalfood" <?php echo (in_array('halalfood',$dat)) ?  'selected':''; ?>>Halal Food</option>
                            <option value="fastdelivery" <?php echo (in_array('fastdelivery',$dat)) ?  'selected':''; ?>>Super Fast Delivery</option>
                            <option value="mealbox" <?php echo (in_array('mealbox',$dat)) ?  'selected':''; ?>>MealBox</option>

                        </select>
                    </div>

                    <div class="form-group">
                        <label >Offer Title</label>
                        <input type="text" name="offer_title" value="{{$data->offer_title}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Offer Value</label>
                        <input type="text" name="offer_value" value="{{$data->offer_value}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Offer Type</label>
                        <select name="offer_type" class="form-control">
                            <option value="offer"{{$data['offer_type']=='offer' ? 'selected':''}}>Offer</option>
                            <option value="exclusive"{{$data['offer_type']=='exclusive' ? 'selected':''}}>Exclusive</option>
                            <option value="promoted"{{$data['offer_type']=='promoted' ? 'selected':''}}>Promoted</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="0"{{($data['recommended'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['recommended'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" name="show_in_home" value="{{$data->show_in_home}}">Show In HomePage</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update Restaurant Info</button>
                    </div>

                </form>

                <script src="http://maps.googleapis.com/maps/api/js?libraries=places" type="text/javascript"></script>
                <script type="text/javascript">
                    function initialize() {
                        var input = document.getElementById('searchTextField');
                        var autocomplete = new google.maps.places.Autocomplete(input);
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var place = autocomplete.getPlace();

                            document.getElementById('latitude').value = place.geometry.location.lat();
                            document.getElementById('longitude').value = place.geometry.location.lng();


                        });
                    }
                    google.maps.event.addDomListener(window, 'load', initialize);
                </script>

            </div>
        </div>
    </div>

@endsection

