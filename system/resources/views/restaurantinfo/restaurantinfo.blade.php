@extends('layouts.app')
@section('title','Restaurant Info')
@section('breadcrumb')
    <h1>
        Restaurant Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>RestaurantInfo Table<span class="pull-right"><a href="{{route('restaurantinfo.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Logo</th>
                        <th>Image</th>
                        <th>Restaurant Type</th>
                        <th>Custom Field Name</th>
                        <th>Custom Field Value</th>
                        <th>Delivery Time</th>
                        <th>Srevice Charge</th>
                        <th>Vat</th>
                        <th>Restaurant Category</th>
                        <th>Offer Title</th>
                        <th>Offer Value</th>
                        <th>Offer Type</th>
                        <th>Recommended</th>
                        <th>Show In Home</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->address}}</td>
                            <td><img src="{{asset('assets/images/restaurant/logo/'.$d->logo)}}" class="img-thumbnail" /></td>
                            <td><img src="{{asset('assets/images/restaurant/background/'.$d->bgimage)}}" class="img-thumbnail" /></td>
                            <td>{{$d->restaurant_type}}</td>
                            <td>{{$d->custom_field_name}}</td>
                            <td>{{$d->custom_field_value}}</td>
                            <td>{{$d->delivery_time}}</td>
                            <td>{{$d->service_charge}}</td>
                            <td>{{$d->vat}}</td>
                            <td>
                                <?php
                                    $rest=json_decode($d->restaurant_category);
                                    echo implode("<br>",$rest);?>
                            </td>
                            <td>{{$d->offer_title}}</td>
                            <td>{{$d->offer_value}}</td>
                            <td>{{$d->offer_type}}</td>
                            <td>{{$d->recommended}}</td>
                            <td>{{$d->show_in_home}}</td>
                            <td><a href="{{route('restaurantinfo.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{route('restaurantinfo.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php $i++; ?>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
