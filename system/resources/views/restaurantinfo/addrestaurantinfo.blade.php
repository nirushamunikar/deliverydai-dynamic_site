@extends('layouts.app')
@section('title','Restaurant Info')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Add Restaurant Info</h1>
            <div class="col-md-9">
                <form action="{{route('restaurantinfo.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Address</label>
                        <input id="searchTextField" type="text" name="address" class="form-control" autocomplete="on" runat="server" placeholder="Enter your address">
                    </div>


                    <input type="hidden" name="latitude" id="latitude" class="form-control ">

                    <input type="hidden" name="longitude" id="longitude" class="form-control ">


                    <div class="form-group ">
                        <label>Logo</label>
                        <input type="file" name="logo" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Background Image</label>
                        <input type="file" name="bgimage" class="form-group ">
                    </div>

                    <div class="form-group ">
                        <label>Restaurant Type</label>
                        <input type="text" name="restaurant_type" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Custom Field Name</label>
                        <input type="text" name="custom_field_name" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Custom Field Value</label>
                        <input type="text" name="custom_field_value" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Delivery Time</label>
                        <input type="text" name="delivery_time" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Service Charge</label>
                        <input type="text" name="service_charge" class="form-control ">
                    </div>


                    <div class="form-group ">
                        <label>Vat</label>
                        <input type="text" name="vat" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Restaurant Category</label>
                        <select name="restaurant_category[]" class="form-control selectpicker" multiple>
                            <option value="pickofthepack">Pick of the Pack</option>
                            <option value="vegoption">Veg Option</option>
                            <option value="halalfood">Halal Food</option>
                            <option value="fastdelivery">Super Fast Delivery</option>
                            <option value="mealbox">MealBox</option>
                        </select>
                    </div>

                    <div class="form-group ">
                        <label>Offer Title</label>
                        <input type="text" name="offer_title" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Offer Value</label>
                        <input type="text" name="offer_value" class="form-control ">
                    </div>

                    <div class="form-group ">
                        <label>Offer Type</label>
                        <select name="offer_type" class="form-control">
                            <option value="offer">Offer</option>
                            <option value="exclusive">Exclusive</option>
                            <option value="promoted">Promoted</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" name="show_in_home">Show In HomePage</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add Restaurant Info</button>
                    </div>
                </form>

                <script src="http://maps.googleapis.com/maps/api/js?libraries=places" type="text/javascript"></script>
                <script type="text/javascript">
                    function initialize() {
                        var input = document.getElementById('searchTextField');
                        var autocomplete = new google.maps.places.Autocomplete(input);
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var place = autocomplete.getPlace();

                            document.getElementById('latitude').value = place.geometry.location.lat();
                            document.getElementById('longitude').value = place.geometry.location.lng();


                        });
                    }
                    google.maps.event.addDomListener(window, 'load', initialize);
                </script>

            </div>
        </div>
    </div>
@endsection