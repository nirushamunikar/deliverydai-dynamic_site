<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link rel="shortcut icon" href="{{asset('assets/images/images/favico.png')}}"/>
    <title>Delivery Dai</title>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyAUHR8odNObqRTN2szNaCSZ3KtQTU1nuK8"></script>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light bg-light navbar-inner">
    <div class="container">
        <div class="navbar-brand">
            <a class="logo" href="#">
                <img src="{{asset('assets/images/images/logo.png')}}" alt="">
            </a>
            <a href="#" class="text">
                <span class="city">Baneshwor</span> Kathmandu, Nepal <i class="fa fa-angle-down"></i>
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav mr-0">
                <li class="nav-item">
                    <a class="nav-link" href="underconstruction/index.html" data-modal="#search-modal">
                        <img src="{{asset('assets/images/images/ic_search.png')}}" alt=""/>
                        Search
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="underconstruction/index.html">
                        <img src="{{asset('assets/images/images/ic_support.png')}}" alt=""/>
                        Support
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="underconstruction/index.html" data-modal="#login-modal">
                        <img src="{{asset('assets/images/images/ic_user.png')}}" alt=""/>
                        Sign In
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-target="navbar-cart" href="underconstruction/index.html">
                        <img src="{{asset('assets/images/images/ic_cart.png')}}" alt=""/>
                        Cart
                    </a>
                    <!-- show when cart is empty -->
                    <div class="navbar-cart navbar-cart-none d-none" id="navbar-cart">
                        <div class="content">
                            <img src="{{asset('assets/images/images/ic_cart.png')}}" alt=""/> Your cart is empty
                        </div>
                    </div>
                    <!-- /.cart -->

                    <!-- show when cart is filled -->
                    <div class="navbar-cart navbar-cart-filled" id="navbar-cart">
                        <div class="content">
                            <h4>My Food Bag</h4>
                            <ul>
                                <li>
                                    <div class="image">
                                        <img src="{{asset('assets/images/images/favorite-3.jpg')}}" alt="" />
                                    </div>
                                    <div class="text">
                                        <a href="#" class="title">KFC  Double Spicy and Double  Chicken(Durbar Margh)</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="image">
                                        <img src="{{asset('assets/images/images/favorite-3.jpg')}}" alt="" />
                                    </div>
                                    <div class="text">
                                        <a href="#" class="title">KFC  Double Spicy (Durbar Margh)</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="image">
                                        <img src="{{asset('assets/images/images/favorite-3.jpg')}}" alt="" />
                                    </div>
                                    <div class="text">
                                        <a href="#" class="title">KFC  Double Spicy (Durbar Margh)</a>
                                    </div>
                                </li>
                            </ul>
                            <table>
                                <tr>
                                    <td>1x</td>
                                    <td>KFC Chicken</td>
                                    <td><div class="price">Rs. 250</div></td>
                                    <td><div class="cross">X</div></td>
                                </tr>
                                <tr>
                                    <td>1x</td>
                                    <td>KFC Chicken</td>
                                    <td><div class="price">Rs. 250</div></td>
                                    <td><div class="cross">X</div></td>
                                </tr>
                            </table>


                            <ul class="total">
                                <li>
                                    <div class="title">SUB TOTAL</div>
                                    <div class="price">715.00</div>
                                </li>
                                <li>
                                    <div class="title">SERVICE CHARGE</div>
                                    <div class="price">10.00</div>
                                </li>
                                <li>
                                    <div class="title">VAT</div>
                                    <div class="price">13.00</div>
                                </li>
                                <li>
                                    <div class="title">DELIVERY CHARGE <a>(Show Estimated)</a></div>
                                    <div class="price">TBD</div>
                                </li>
                                <li>
                                    <div class="title">NET PAYABLE </div>
                                    <div class="price">715.00</div>
                                </li>

                            </ul>

                            <a href="" class="btn btn-orange">Proceed To Checkout</a>
                        </div>
                    </div>

                </li>
            </ul>

        </div>
    </div>
</nav>
<!-- /.navbar-inner -->

<div class="banner-offer">
    <div class="container">
        <div class="row slide-item-wrapper">
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-1.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-2.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-3.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-1.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-2.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 slide-item">
                <a href="#" class="item">
                    <img src="{{asset('assets/images/images/offer-3.jpg')}}" alt="">
                </a>
                <!-- /.item -->
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->
    </div>
</div>
<!-- /.banner-offer -->

<div class="restaurant-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="restaurant-content-sidebar" id="sticky-sidebar">
                    <ul >
                        <li class="active">
                            <a href="#restaurant">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_restaurant.png')}}" alt="">
                                    </span>
                                Restaurant Near You
                            </a>

                        </li>
                        <li>
                            <a href="#offers">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_offer.png')}}" alt="">
                                    </span>
                                Offers Around You
                            </a>

                        </li>
                        <li>
                            <a href="#pick">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_favorite.png')}}" alt="">
                                    </span>
                                Pick Of the Pack
                            </a>

                        </li>
                        <li>
                            <a href="#veg">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_veg.png')}}" alt="">
                                    </span>
                                Vegeterian Option
                            </a>

                        </li>
                        <li>
                            <a href="#delivery">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_delivery.png')}}" alt="">
                                    </span>
                                Superfast Delivery
                            </a>
                        </li>
                        <li>
                            <a href="#meal">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_meal.png')}}" alt="">
                                    </span>
                                Mealbox Favorite
                            </a>
                        </li>
                        <li>
                            <a href="#halal">
                                        <span class="image">
                                            <img src="{{asset('assets/images/images/ic_halal.png')}}" alt="">
                                        </span>
                                Halal Food
                            </a>
                        </li>
                        <li>
                            <a href="#all">
                                    <span class="image">
                                        <img src="{{asset('assets/images/images/ic_explore.png')}}" alt="">
                                    </span>
                                Explore All
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.restaurant-content-sidebar -->
            </div>
            <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="restaurant-content-list">
                    <div id="restaurant">
                        <h2 class="section-inner-header" >Restaurant Near You</h2>

                        <div class="restaurant-list row">
                            <div class="col-md-4">
                                <div class="item ">
                                    <span class="offer orange">Exclusive</span>
                                    <a href="#" class="image">
                                        <img src="{{asset('assets/images/images/corner-house.png')}}" alt="">
                                    </a>
                                    <div class="text">
                                        <h4><a href="#" class="title">Corner House Ice Cream</a></h4>
                                        <p class="category">Desserts</p>
                                        <div class="details">
                                            <div class="price"><span>Rs. 250</span> / pieces</div>
                                            <div class="time"><i class="fa fa-clock"></i><span>30</span> Min.</div>
                                        </div>
                                        <!-- /.details -->
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.item -->
                            </div>
                        </div>
                        <!-- /.restaurant-list -->
                    </div>
                    <div class="dash-separator"></div>
                    <div id="offers">
                        <h2 class="section-inner-header">Offers Around You</h2>

                        <div class="restaurant-list row">
                            <div class="col-md-4">
                                <div class="item ">
                                    <span class="offer orange">Exclusive</span>
                                    <a href="#" class="image">
                                        <img src="{{asset('assets/images/images/corner-house.png')}}" alt="">
                                    </a>
                                    <div class="text">
                                        <h4><a href="#" class="title">Corner House Ice Cream</a></h4>
                                        <p class="category">Desserts</p>
                                        <div class="details">
                                            <div class="price"><span>Rs. 250</span> / pieces</div>
                                            <div class="time"><i class="fa fa-clock"></i><span>30</span> Min.</div>
                                        </div>
                                        <!-- /.details -->
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.item -->
                            </div>

                        </div>
                        <!-- /.restaurant-list -->
                    </div>
                    <div class="dash-separator"></div>
                    <div  id="pick">
                        <h2 class="section-inner-header">Pick of the Pack</h2>

                        <div class="restaurant-list row">
                            @foreach($picks as $pick)
                                <?php $old=json_decode($pick->restaurant_category)?>
                                    @if((in_array('pickofthepack',$old)))
                                        <div class="col-md-4">
                                            <div class="item ">
                                                <span class="offer orange">Exclusive</span>
                                                <a href="#" class="image">
                                                    <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                                </a>
                                                <div class="text">
                                                    <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                                    <p class="category">{{$pick->restaurant_type}}</p>
                                                    <div class="details">
                                                        <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                                        <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span></div>
                                                    </div>
                                                    <!-- /.details -->
                                                </div>
                                                <!-- /.text -->
                                            </div>
                                            <!-- /.item -->
                                        </div>
                                    @endif
                            @endforeach
                        </div>
                        <!-- /.restaurant-list -->
                    </div>
                    <div class="dash-separator"></div>
                    <div id="veg">
                        <h2 class="section-inner-header">Vegeterian Options</h2>
                        <div class="restaurant-list row">
                            @foreach($picks as $pick)
                                <?php $old=json_decode($pick->restaurant_category)?>
                                @if((in_array('vegoption',$old)))
                                    <div class="col-md-4">
                                        <div class="item ">
                                            <span class="offer orange">Exclusive</span>
                                            <a href="#" class="image">
                                                <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                            </a>
                                            <div class="text">
                                                <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                                <p class="category">{{$pick->restaurant_type}}</p>
                                                <div class="details">
                                                    <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                                    <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span></div>
                                                </div>
                                                <!-- /.details -->
                                            </div>
                                            <!-- /.text -->
                                        </div>
                                    <!-- /.item -->
                                    </div>
                                @endif
                            @endforeach



                        <!-- /.restaurant-list -->
                    </div>
                    <div class="dash-separator"></div>
                    <div id="delivery">
                        <h2 class="section-inner-header">Superfast Delivery</h2>
                        <div class="restaurant-list row">
                            @foreach($picks as $pick)
                                <?php $old=json_decode($pick->restaurant_category)?>
                                @if((in_array('fastdelivery',$old)))
                                    <div class="col-md-4">
                                        <div class="item ">
                                            <span class="offer orange">Exclusive</span>
                                            <a href="#" class="image">
                                                <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                            </a>
                                            <div class="text">
                                                <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                                <p class="category">{{$pick->restaurant_type}}</p>
                                                <div class="details">
                                                    <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                                    <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span></div>
                                                </div>
                                                <!-- /.details -->
                                            </div>
                                            <!-- /.text -->
                                        </div>

                                        <!-- /.item -->
                                    </div>
                                 @endif
                            @endforeach
                        </div>
                        <!-- /.restaurant-list -->
                    </div>
                    <div class="dash-separator"></div>
                    <div id="meal">
                        <h2 class="section-inner-header" >Mealbox Favorite</h2>
                            <div class="restaurant-list row">
                                @foreach($picks as $pick)
                                    <?php $old=json_decode($pick->restaurant_category)?>
                                    @if((in_array('mealbox',$old)))
                                        <div class="col-md-4">
                                            <div class="item ">
                                                <span class="offer orange">Exclusive</span>
                                                <a href="#" class="image">
                                                    <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                                </a>
                                                <div class="text">
                                                    <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                                    <p class="category">{{$pick->restaurant_type}}</p>
                                                    <div class="details">
                                                        <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                                        <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span> Min.</div>
                                                    </div>
                                                    <!-- /.details -->
                                                </div>
                                                <!-- /.text -->
                                            </div>
                                        <!-- /.item -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        <!-- /.restaurant-list -->
                    </div>
                    <div id="halal">
                        <h2 class="section-inner-header" >Halal Food</h2>
                        <div class="restaurant-list row">
                            @foreach($picks as $pick)
                                <?php $old=json_decode($pick->restaurant_category)?>
                                @if((in_array('halalfood',$old)))
                                    <div class="col-md-4">
                                        <div class="item ">
                                            <span class="offer orange">Exclusive</span>
                                            <a href="#" class="image">
                                                <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                            </a>
                                            <div class="text">
                                                <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                                <p class="category">{{$pick->restaurant_type}}</p>
                                                <div class="details">
                                                    <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                                    <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span></div>
                                                </div>
                                                <!-- /.details -->
                                            </div>
                                            <!-- /.text -->
                                        </div>

                                        <!-- /.item -->
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <!-- /.restaurant-list -->
                    </div>
                </div>
                <!-- /.restaurant-content-lists -->
            </div>
            <!-- /.col-md-9 -->

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="restaurant-content-list">
                <div class="dash-separator"></div>
                <h2 class="section-inner-header" id="all">Others</h2>

                <div class="restaurant-list row">
                    @foreach($picks as $pick)
                        <?php $old=json_decode($pick->restaurant_category)?>
                            <div class="col-md-3">
                                <div class="item ">
                                    <span class="offer orange">Exclusive</span>
                                    <a href="#" class="image">
                                        <img src="{{asset('assets/images/restaurant/background/'.$pick->bgimage)}}" alt="">
                                    </a>
                                    <div class="text">
                                        <h4><a href="#" class="title">{{$pick->name}}</a></h4>
                                        <p class="category">{{$pick->restaurant_type}}</p>
                                        <div class="details">
                                            <div class="price"><span>{{$pick->customvalue}}</span> / {{$pick->customname}}</div>
                                            <div class="time"><i class="fa fa-clock"></i><span>{{$pick->delivery_time}}</span></div>
                                        </div>
                                        <!-- /.details -->
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.item -->
                            </div>
                        @endforeach

                </div>
                <!-- /.restaurant-list -->
            </div>
        </div>
    </div>
</div>
<!-- /.restaurant-content -->

<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <h2>Help & Support</h2>
                <ul>
                    <li><a href="underconstruction/index.html">Help Center</a></li>
                    <li><a href="underconstruction/index.html">Services Areas</a></li>
                    <li><a href="underconstruction/index.html">Contact Support</a></li>
                    <li><a href="underconstruction/index.html">Privacy & Terms</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>DeliveryDai</h2>
                <ul>
                    <li><a href="underconstruction/index.html">About Us</a></li>
                    <li><a href="underconstruction/index.html">How we work?</a></li>
                    <li><a href="underconstruction/index.html">Partners</a></li>
                    <li><a href="underconstruction/index.html">Become A Rider</a></li>
                    <li><a href="underconstruction/index.html">For Restaurant Owners</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>Offers</h2>
                <ul>
                    <li><a href="underconstruction/index.html">Subscribe</a></li>
                    <li><a href="underconstruction/index.html">Offers</a></li>
                    <li><a href="underconstruction/index.html">Gift Cards</a></li>
                    <li><a href="underconstruction/index.html">Payment</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>Contact us</h2>
                <ul>
                    <li><a href="underconstruction/index.html"><i class="fa fa-phone"></i> 01-45453828</a></li>
                    <li><a href="underconstruction/index.html"><i class="fa fa-envelope"></i> info@deliveryDai.com</a></li>

                </ul>
                <ul class="social-icons">
                    <li>
                        <a href="https://facebook.com/">
                            <span>facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/">
                            <span>twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/">
                            <span>instagram</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://google.com/">
                            <span>google</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
        </div>
        <!-- /.row -->
    </div>
    <div class="copyright">
        <div class="container">
            <p>&copy; 2018. DeliveryDai.com</p>
            <p>
                Design & Developed with <i class="fa fa-heart beat  color-1"></i><a href="http://www.prabidhilabs.com"> Prabidhilabs</a>
            </p>
        </div>
    </div>
</section>



<div class="custom-modal" id="search-modal">
    <div class="custom-modal-overlay"></div>
    <div class="custom-modal-content">
        <div class="custom-modal-close">
            <img src="images/icon-close.svg" alt="">
            <span>Close</span>
        </div>
        <form method="get">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter your search keywords"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn"><img src="images/ic_search.png" alt=""></button>
                </div>
            </div>
            <p>Search for your favorite dish or restaurant.</p>
        </form>
    </div>
</div>
<!-- /,custom-modal -->

<div class="custom-modal custom-modal-sm" id="login-modal">
    <div class="custom-modal-overlay"></div>
    <div class="custom-modal-content">
        <div class="custom-modal-close">
            <img src="images/icon-close-white.svg" alt="">
            <span>Close</span>
        </div>
        <form method="post" action="#">
            <h2>Login</h2>
            <p>Enter your login details below:</p>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Phone Number or Email Address"/>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Enter Password"/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-orange" value="Login">
            </div>
            <p>Can't access your account? <a href="#">Reset Password</a</p>
            <div class="separator"><span>OR</span></div>
            <div class="btn-group">
                <a href="#" class="btn btn-facebook">Login with facebook</a>
                <a href="#" class="btn btn-google-plus">Login with google plus</a>
            </div>


        </form>
        <p>New User? Click here to <a href="#">Signup</a></p>
    </div>
</div>
<!-- /.custom-modal -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js" ></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="{{asset('assets/js/sticky.js')}}js/"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
<!-- chat plugin -->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'ffef850c-5ad6-4d6e-a229-c9cda684846d', f: true }); done = true; } }; })();</script>
</body>
</html>