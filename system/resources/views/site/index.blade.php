<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/images/favico.png')}}"/>
    <title>Delivery Dai</title>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyAUHR8odNObqRTN2szNaCSZ3KtQTU1nuK8"></script>
</head>
<body>

<section class="header-banner">

    <div class="image">

        <img src="{{asset('assets/images/images/banner.jpg')}}" alt="">
        <video poster="{{asset('assets/images/images/banner.jpg')}}" type="video/mp4" autoplay="autoplay">
            <source src="{{asset('assets/images/images/banner-video.mp4')}}" type="video/mp4" />
        </video>

    </div>
    <div class="text">
        @foreach($homepanels as $panel)
        <div class="logo">
            <img src="{{asset('assets/images/homepanel/logo/'.$panel->logo)}}" alt="">
        </div>
    @endforeach
        <!-- /.logo -->
        <div class="text-content">
            <div id="holder">
                <div id="slogans">
                    @foreach($fliptexts as $flip)
                    <p class="slogan"><strong>{{$flip->text}}</strong></p>
                   @endforeach
                </div>
            </div>
            @foreach($homepanels as $panel)
            <p>
               {{$panel->caption}}
            </p>
            <div class="searh-form">
                <form class="custom-form form-inline">
                    <div class="form-group">

                        <div class="location"onclick="getLocation()"><i class="fa fa-crosshairs"></i> Locate Me</div>
                        <input type="text" class="form-control w-100" placeholder="Enter your delivery location" onFocus="geolocate()" id="autocomplete">

                        <input type="submit" class="btn btn-custom" value="Find Food">
                    </div>

                </form>
                <!-- /.custom-form -->
            </div>
            <div class="phone">
                01-41234567
                <span>Call us now</span>
            </div>
            <div class="buttons">
                <a href="" class="btn btn-ghost">Login</a>
                <a href="" class="btn btn-orange">Signup</a>
            </div>
        </div>
        <ul class="social-icons">
            <li>
                <a href="{{$panel->twitterlink}}">
                    <span>Twitter</span>
                </a>
            </li>
            <li>
                <a href="{{$panel->fblink}}">
                    <span>facebook</span>
                </a>
            </li>
            <li>
                <a href="{{$panel->instalink}}">
                    <span>instagram</span>
                </a>
            </li>
            <li>
                <a href="{{$panel->googlelink}}">
                    <span>google</span>
                </a>
            </li>
        </ul>
    </div>
    @endforeach

    <div class="scroll">
        <i class="fa fa-angle-down"></i>
    </div>
</section>
<!-- /.header-banner -->
<nav class="navbar sticky-navbar navbar-expand-md navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="{{asset('assets/images/images/logo.png')}}" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav mr-0">
                <li class="nav-item active">
                    <a class="nav-link" href="underconstruction/index.html">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Food</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Restaurants</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Testimonials</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Services</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Orders</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="underconstruction/index.html">Contact</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link btn btn-custom" href="underconstruction/index.html">Order Now</a>
                </li>


            </ul>

        </div>
    </div>
</nav>

<section class="separator">
    <div class="image">
        <img src="{{asset('assets/images/images/fork-knife.png')}}" alt="">
    </div>
</section>
<!-- /.separtor -->
<section class="section section-favorite">
    <div class="section-image">
        <img src="{{asset('assets/images/images/left-food-banner.png')}}" alt="" class="img-left">
        <img src="{{asset('assets/images/images/right-food-banner.png')}}" alt="" class="img-right">
    </div>
    <div class="container">
        <h2 class="section-header">Recent Customer <span>Favorite</span></h2>
        <div class="row favorite-row">
            @foreach($menus as $menu)
            <div class="col-lg-4 col-sm-6 favorite-item">
                <div class="item">
                    <a href="underconstruction/index.html" class="image">
                        <img src="{{asset('assets/images/restaurant/menu/'.$menu->photo)}}" alt="">
                    </a>
                    <!-- /.image -->
                    <div class="text">
                        <a href="underconstruction/index.html" class="title">{{$menu->name}}</a>
                        <div class="price">
                            Rs{{$menu->sale_price}} <span>/{{$menu->unit}}</span>
                        </div>
                        <a href="underconstruction/index.html" class="btn btn-orange">Order Now</a>
                    </div>
                    <!-- /.text -->
                </div>
                <!-- /.item -->
            </div>
            @endforeach
            <!-- /.col-lg-4 -->

        </div>
        <!-- /.row -->

        <a href="underconstruction/index.html" class="btn btn-all btn-custom">View All</a>
    </div>
</section>
<!-- /.favorite -->
<section class="section section-popular">
    <div class="container">
        <h2 class="section-header">
            Choose From Most <span>Popular</span>
        </h2>
        <div class="row">
            @foreach($resinfos as $res)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <div class="item">
                    <div class="image">
                        <a href="{{route('detail',$res->id)}}">
                            <img src="{{asset('assets/images/restaurant/logo/'.$res->logo)}}" alt="{{$res->name}}">
                        </a>
                    </div>
                    <!-- /.image -->
                    <div class="text">{{$res->name}}</div>
                </div>

                <!-- /.item -->
            </div>
        @endforeach
            <!-- /.col-6 col-sm-4 col-md-3 col-lg-2 -->

        </div>
        <!-- /.row -->
        <div class="total">
            <div class="number">
                <span> 210 Foods & Restaurants</span>
            </div>
            <!-- /.number -->
            <div class="button">
                <a href="underconstruction/index.html">View All</a>
            </div>
        </div>
        <!-- /.total -->
    </div>
    <!-- /.container -->
</section>
<!-- /.popular -->

<section class="section section-findings">
    <div class="container">
        <h2 class="section-header">Findings Made <span>Easy</span></h2>
        <p>
            Search for your favorite food from your favorite restaurant easily and now even faster.<br/> We’ve got food menu from more than 200 restaurant just for you.
        </p>

        <div class="searh-form">
            <form class="custom-form form-inline">
                <div class="left-btn"><i class="fa fa-map-marker"></i></div>
                <input type="text" class="form-control" placeholder="Find restaurant near you"/>
                <input type="submit" class="btn btn-custom" value="Search"/>
            </form>
        </div>
        <div class="text">
            <div class="row">
                <div class="col-md-7">
                    <h4 class="title">How DeliveryDai Works?</h4>
                    <ul class="row">
                        <li class="col-6">1. Search for  Food / Restaurant</li>
                        <li class="col-6">2. Select Food Item</li>
                        <li class="col-6">3. Press the order button</li>
                        <li class="col-6">4. Fill in your details.</li>
                        <li class="col-6">5. Select your payment method.</li>
                        <li class="col-6">6. Wait for Delivery Dai at your door.</li>
                    </ul>
                </div>
                <!-- /.col-md-7 -->
                <div class="col-md-5">
                    <h4 class="title">Confused or didn’t find what your are looking for?</h4>
                    <div class="contact">
                        <div class="image"><img src="{{asset('assets/images/images/iphone.png')}}" alt=""></div>
                        <div class="text">
                            <h4>Fell Free to Call us at:</h4>
                            <p>43123456 / 9841234567</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
</section>
<!-- /.findings -->

<section class="section section-offer">
    <div class="container">
        <div class="item">
            <h2>
                Delivery Day Now Delivers <br/>
                Happiness in Your Office
            </h2>
            <p>
                Get <span>25%</span>  discount on every call.
            </p>
            <a href="underconstruction/index.html" class="btn btn-orange">Click to know more.</a>
        </div>
        <!-- /.item -->
    </div>
</section>
<!-- /.section-offer -->
<div class="section-quick-search">
    <div class="container">
        <div class="content">
            <h4>Quick Searches</h4>
            <p>Discover Restaurants by type of meal</p>
            <div class="search-row">
                <div class="item">
                    <a href="underconstruction/index.html">
                <span class="image">
                  <img src="{{asset('assets/images/images/breakfast.png')}}" alt="">
                </span>
                        Breakfast
                    </a>
                </div>
                <div class="item">
                    <a href="underconstruction/index.html">
                <span class="image">
                    <img src="{{asset('assets/images/images/lunch.png')}}" alt="">
                </span>
                        Lunch
                    </a>
                </div>
                <div class="item">
                    <a href="underconstruction/index.html">
                <span class="image">
                    <img src="{{asset('assets/images/images/dinner.png')}}" alt="">
                </span>
                        Dinner
                    </a>
                </div>
                <div class="item">
                    <a href="underconstruction/index.html">
                <span class="image">
                    <img src="{{asset('assets/images/images/delivery.png')}}" alt="">
                </span>
                        Delivery
                    </a>
                </div>
                <div class="item">
                    <a href="underconstruction/index.html">
                  <span class="image">
                      <img src="{{asset('assets/images/images/drinks.png')}}" alt="">
                  </span>
                        Drinks & Night Life
                    </a>
                </div>
                <div class="item">
                    <a href="underconstruction/index.html">
                  <span class="image">
                      <img src="{{asset('assets/images/images/takeaway.png')}}" alt="">
                  </span>
                        Take out
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section section-testimonials">
    <div class="container">
        <h2 class="section-header">
            <img src="{{asset('assets/images/images/fork-knife-white.png')}}" alt=""/> Happy Customers <img src="{{asset('assets/images/images/happy.png')}}" alt="">
        </h2>
        <div class="content">
            @foreach($testimonials as $testi)
            <div class="item">
                <div class="image">
                    <img src="{{asset('assets/images/testimonial/'.$testi->photo)}}" alt="">
                </div>
                <!-- /.image -->
                <div class="text">
                    <div class="name">{{$testi->name}} </div>
                    <div class="message">
                        {!! $testi->review !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</section>
<!-- /.testimonials -->
<div class="section section-list">
    <div class="container">

        List your Restaurant at DeliveryDai!<br/>
        <a href="underconstruction/index.html">Click here reach 1,50,000 + new customers.</a>
    </div>
</div>
<!-- /.list -->
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <h2>Help & Support</h2>
                <ul>
                    <li><a href="underconstruction/index.html">Help Center</a></li>
                    <li><a href="underconstruction/index.html">Services Areas</a></li>
                    <li><a href="underconstruction/index.html">Contact Support</a></li>
                    <li><a href="underconstruction/index.html">Privacy & Terms</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>DeliveryDai</h2>
                <ul>
                    <li><a href="underconstruction/index.html">About Us</a></li>
                    <li><a href="underconstruction/index.html">How we work?</a></li>
                    <li><a href="underconstruction/index.html">Partners</a></li>
                    <li><a href="underconstruction/index.html">Become A Rider</a></li>
                    <li><a href="underconstruction/index.html">For Restaurant Owners</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>Offers</h2>
                <ul>
                    <li><a href="underconstruction/index.html">Subscribe</a></li>
                    <li><a href="underconstruction/index.html">Offers</a></li>
                    <li><a href="underconstruction/index.html">Gift Cards</a></li>
                    <li><a href="underconstruction/index.html">Payment</a></li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-sm-6 col-md-3">
                <h2>Contact us</h2>
                <ul>
                    <li><a href="underconstruction/index.html"><i class="fa fa-phone"></i> 01-45453828</a></li>
                    <li><a href="underconstruction/index.html"><i class="fa fa-envelope"></i> info@deliveryDai.com</a></li>

                </ul>
                <ul class="social-icons">
                    <li>
                        <a href="https://facebook.com/">
                            <span>facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/">
                            <span>twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/">
                            <span>instagram</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://google.com/">
                            <span>google</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.col-md-3 -->
        </div>
        <!-- /.row -->
    </div>
    <div class="copyright">
        <div class="container">
            <p>&copy; 2018. DeliveryDai.com</p>
            <p>
                Design & Developed with <i class="fa fa-heart beat  color-1"></i><a href="http://www.prabidhilabs.com"> Prabidhilabs</a>
            </p>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
<!-- chat plugin -->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'ffef850c-5ad6-4d6e-a229-c9cda684846d', f: true }); done = true; } }; })();</script>

<script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7R6XWDuCnvNSqeAE8d0TqoMiLSCiSctQ&libraries=places&callback=initAutocomplete"
        async defer></script>

</body>
</html>