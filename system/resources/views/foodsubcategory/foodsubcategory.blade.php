
@extends('layouts.app')
@section('title','FoodSubCategory')
@section('breadcrumb')
    <h1>
        FoodSubCategory Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>FoodSubCategory <span class="pull-right"><a href="{{route('foodsubcategory.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->name}}</td>
                            <td><a href="{{route('foodsubcategory.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('foodsubcategory.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
