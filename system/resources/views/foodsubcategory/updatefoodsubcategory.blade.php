@extends('layouts.app');
@section('title','FoodSubCategory')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit FoodSubCategory</h2>
            <div class="col-md-9">

                <form action="{{route('foodsubcategory.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="name" value="{{$data->name}}" class="form-control">
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label >Food Category</label>--}}
                        {{--<select name="category_id" id="" class="form-control">--}}
                            {{--@foreach($foodcats as $cat)--}}
                                {{--<option value="{{$cat->id}}" {{($data->category_id == $cat->id) ? 'selected':''}} >{{$cat->name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update FoodSubCategory</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection