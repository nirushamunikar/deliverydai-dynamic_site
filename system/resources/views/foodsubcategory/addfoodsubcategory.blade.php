@extends('layouts.app')
@section('title','FoodSubCategory')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add FoodSubCategory</h2>
            <div class="col-md-9">
                <form action="{{route('foodsubcategory.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name"class="form-control">
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label>Food Category</label>--}}
                        {{--<select name="category_id" id="" class="form-control">--}}
                            {{--@foreach($foodcats as $cat)--}}
                                {{--<option value="{{$cat->id}}">{{$cat->name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add FoodSubCategory</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


