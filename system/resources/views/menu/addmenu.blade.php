@extends('layouts.app')
@section('title','Menu')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add Menu</h2>
            <div class="col-md-9">
                <form action="{{route('menu.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Restaurant Name</label>
                        <select name="restaurant_id" id="" class="form-control">
                            @foreach($restinfo as $rest)
                                <option value="{{$rest->id}}">{{$rest->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Food</label>
                        <select name="food_id" id="" class="form-control">
                            @foreach($foods as $food)
                                <option value="{{$food->id}}">{{$food->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group ">
                        <label>Photo</label>
                        <input type="file" name="photo" class="form-group ">
                    </div>

                    <div class="form-group">
                        <label>Unit</label>
                        <input type="text" name="unit" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Sale Price</label>
                        <input type="text" name="sale_price" id="sale_price" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Offer Price</label>
                        <input type="text" name="offer_price" class="form-control">
                    </div>

                    <label >Offer</label>
                    <select name="offer" class="form-control">
                        <option value="bestseller">Best Seller</option>
                        <option value="exclusive">Exclusive</option>
                        <option value="musttry">MustTry</option>
                    </select>


                    <div class="form-group">
                        <label>Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add Menu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


