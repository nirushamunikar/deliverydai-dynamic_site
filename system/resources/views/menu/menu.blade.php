@extends('layouts.app')
@section('title','Menu')
@section('breadcrumb')
    <h1>
        Menu Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Menu <span class="pull-right"><a href="{{route('menu.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Restaurant Name</th>
                        <th>Food Name</th>
                        <th>Photo</th>
                        <th>Unit</th>
                        <th>Sale Price</th>
                        <th>Offer Price</th>
                        <th>Offer</th>
                        <th>Recommended</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->restaurant_id}}</td>
                            <td>{{$d->food_id}}</td>
                            <td><img src="{{asset('assets/images/restaurant/menu/'.$d->photo)}}" class="img-thumbnail" style="height: 100px; width: 150px;"/></td>
                            <td>{{$d->unit}}</td>
                            <td>{{$d->sale_price}}</td>
                            <td>{{$d->offer_price}}</td>
                            <td>{{$d->offer}}</td>
                            <td>{{$d->recommended}}</td>
                            <td><a href="{{route('menu.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('menu.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
