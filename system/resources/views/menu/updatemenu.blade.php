@extends('layouts.app');
@section('title','Menu')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit Menu</h2>
            <div class="col-md-9">

                <form action="{{route('menu.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Restaurant Name</label>
                        <select name="restaurant_id" id="" class="form-control">
                            @foreach($restinfo as $rest)
                                <option value="{{$rest->id}}" {{($data->restaurant_id == $rest->id) ? 'selected':''}} >{{$rest->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Food</label>
                        <select name="food_id" id="" class="form-control">
                            @foreach($foods as $food)
                                <option value="{{$food->id}}" {{($data->food_id == $food->id) ? 'selected':''}} >{{$food->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Photo</label><br/>
                        <img src="{{asset('assets/images/restaurant/menu/'.$data->photo)}}" height="150px"; width="300px">
                        <input type="file" name="photo" >
                    </div>

                    <div class="form-group">
                        <label >Unit</label>
                        <input type="text" name="unit" value="{{$data->unit}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Sale Price</label>
                        <input type="text" name="sale_price" value="{{$data->sale_price}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Offer Price</label>
                        <input type="text" name="offer_price" value="{{$data->offer_price}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Offer</label>
                        <select name="offer" class="form-control">
                            <option value="bestseller"{{$data['offer']=='bestseller' ? 'selected':''}}>Best Seller</option>
                            <option value="exclusive"{{$data['offer']=='exclusive' ? 'selected':''}}>Exclusive</option>
                            <option value="musttry"{{$data['offer']=='musttry' ? 'selected':''}}>MustTry</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label >Recommended</label>
                        <select name="recommended" class="form-control">
                            <option value="1"{{($data['recommended'] == 1) ?'selected':''}}>Publish</option>
                            <option value="0"{{($data['recommended'] == 0) ?'selected':''}}>Unpublished</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update Menu</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection