@extends('layouts.app')
@section('title','FoodCategory')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add FoodCategory</h2>
            <div class="col-md-9">
                <form action="{{route('foodcategory.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add FoodCategory</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


