@extends('layouts.app');
@section('title','OpeningHour')
@section('content')

    <div class="container">
        <div class="row">
            <h2>Edit OpeningHour</h2>
            <div class="col-md-9">

                <form action="{{route('openinghour.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label >Restaurant Name</label>
                        <select name="restaurant_id" id="" class="form-control">
                            @foreach($restinfo as $rest)
                                <option value="{{$rest->id}}" {{($data->restaurant_id == $rest->id) ? 'selected':''}} >{{$rest->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label >day</label>
                        <input type="text" name="day" value="{{$data->day}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >open_time</label>
                        <input type="text" name="open_time" value="{{$data->open_time}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >close_time</label>
                        <input type="text" name="close_time" value="{{$data->close_time}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label >Status</label>
                        <select name="status" class="form-control">
                            <option value="0"{{($data['status'] == 0) ?'selected':''}}>Unpublished</option>
                            <option value="1"{{($data['status'] == 1) ?'selected':''}}>Publish</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="update">Update OpeningHour</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection