@extends('layouts.app')
@section('title','OpeningHour')
@section('breadcrumb')
    <h1>
        OpeningHour Page
        <small>it all starts here</small>
    </h1>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>OpeningHour <span class="pull-right"><a href="{{route('openinghour.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></span></h2>
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>S.N</th>
                        <th>Restaurant Name</th>
                        <th>Day</th>
                        <th>Open TIme</th>
                        <th>Close Time</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($data as $d)
                        <tr>

                            <td>{{$i}}</td>
                            <td>{{$d->restaurant_id}}</td>
                            <td>{{$d->day}}</td>
                            <td>{{$d->open_time}}</td>
                            <td>{{$d->close_time}}</td>
                            <td>{{$d->status}}</td>
                            <td><a href="{{route('openinghour.edit',$d->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('openinghour.delete',$d->id)}}" onclick="return confirm('Do you really want to delete this?')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
