@extends('layouts.app')
@section('title','OpeningHour')
@section('content')
    <div class="container">
        <div class="row">
            <h2>Add OpeningHour</h2>
            <div class="col-md-9">
                <form action="{{route('openinghour.save')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Restaurant Name</label>
                        <select name="restaurant_id" id="" class="form-control">
                            @foreach($restinfo as $rest)
                                <option value="{{$rest->id}}">{{$rest->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Day</label>
                        <input type="text" name="day"class="form-control">
                    </div>


                    <div class="form-group">
                        <label>Open Time</label>
                        <input type="text" name="open_time"class="form-control">
                    </div>


                    <div class="form-group">
                        <label>Close Time</label>
                        <input type="text" name="close_time"class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Publish</option>
                            <option value="0">Unpublished</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="save">Add OpeningHour</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


