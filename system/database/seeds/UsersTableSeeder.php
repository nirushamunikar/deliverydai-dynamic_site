<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'first_name'=>'Admin',
            'last_name'=>'Admin',
            'address'=>'test',
            'email' => 'test@deliverydai.com',
            'password' => bcrypt('test')
        ]);
    }
}
