<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('restaurant_id')->nullable();
            $table->string('food_id')->nullable();
            $table->string('photo')->nullable();
            $table->string('unit')->nullable();
            $table->string('sale_price')->nullable();
            $table->string('offer_price')->nullable();
            $table->string('offer')->nullable();
            $table->tinyInteger('recommended')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
