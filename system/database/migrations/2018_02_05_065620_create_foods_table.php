<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('food_category_id')->nullable();
            $table->string('food_subcategory_id')->nullable();
            $table->string('name')->nullable();
            $table->string('food_type_id')->nullable();
            $table->longText('description')->nullable();
            $table->string('fav_count')->nullable();
            $table->tinyInteger('recommended')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
}
