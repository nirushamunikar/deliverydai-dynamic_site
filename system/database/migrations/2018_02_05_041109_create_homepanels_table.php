<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepanels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo');
            $table->string('caption');
            $table->string('bgimage');
            $table->string('videourl')->nullable();
            $table->string('twitterlink');
            $table->string('fblink');
            $table->string('instalink');
            $table->string('googlelink');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepanels');
    }
}
