<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurantinfos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('logo');
            $table->string('bgimage')->nullable();
            $table->string('restaurant_type')->nullable();
            $table->string('custom_field_name')->nullable();
            $table->string('custom_field_value')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('service_charge')->nullable();
            $table->string('vat')->nullable();
            $table->string('restaurant_category')->nullable();
            $table->string('offer_title')->nullable();
            $table->string('offer_value')->nullable();
            $table->string('offer_type')->nullable();
            $table->tinyInteger('recommended')->default('0');
            $table->string('show_in_home')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurantinfos');
    }
}
