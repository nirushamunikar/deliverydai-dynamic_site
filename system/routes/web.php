<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'HomeController@index')->name('login');
Route::post('login', 'HomeController@postLogin')->name('login.post');

Route::group(['middleware' => 'auth'],function(){

    Route::get('logout', 'HomeController@logout')->name('logout');
    Route::get('dashboard', 'HomeController@control')->name('dashboard');

    //Routes for RestaurantInfo table
    Route::get('restaurantinfo','RestaurantInfoController@index')->name('restaurantinfo');
    Route::get('restaurantinfo/add','RestaurantInfoController@add')->name('restaurantinfo.add');
    Route::post('restaurantinfo/save','RestaurantInfoController@save')->name('restaurantinfo.save');
    Route::get('restaurantinfo/edit/{id}','RestaurantInfoController@edit')->name('restaurantinfo.edit');
    Route::post('restaurantinfo/update/{id}','RestaurantInfoController@update')->name('restaurantinfo.update');
    Route::get('restaurantinfo/delete/{id}','RestaurantInfoController@delete')->name('restaurantinfo.delete');

    //Routes For HomePanel Table
    Route::get('homepanel','HomePanelController@index')->name('homepanel');
    Route::get('homepanel/add','HomePanelController@add')->name('homepanel.add');
    Route::post('homepanel/save','HomePanelController@save')->name('homepanel.save');
    Route::get('homepanel/edit/{id}','HomePanelController@edit')->name('homepanel.edit');
    Route::post('homepanel/update/{id}','HomePanelController@update')->name('homepanel.update');
    Route::get('homepanel/delete/{id}','HomePanelController@delete')->name('homepanel.delete');

    //Routes For Fliptext Table
    Route::get('fliptext','FliptextController@index')->name('fliptext');
    Route::get('fliptext/add','FliptextController@add')->name('fliptext.add');
    Route::post('fliptext/save','FliptextController@save')->name('fliptext.save');
    Route::get('fliptext/edit/{id}','FliptextController@edit')->name('fliptext.edit');
    Route::post('fliptext/update/{id}','FliptextController@update')->name('fliptext.update');
    Route::get('fliptext/delete/{id}','FliptextController@delete')->name('fliptext.delete');

    //Routes For Testimonial Table
    Route::get('testimonial','TestimonialController@index')->name('testimonial');
    Route::get('testimonial/add','TestimonialController@add')->name('testimonial.add');
    Route::post('testimonial/save','TestimonialController@save')->name('testimonial.save');
    Route::get('testimonial/edit/{id}','TestimonialController@edit')->name('testimonial.edit');
    Route::post('testimonial/update/{id}','TestimonialController@update')->name('testimonial.update');
    Route::get('testimonial/delete/{id}','TestimonialController@delete')->name('testimonial.delete');

    //Routes For Food Category Table
    Route::get('foodcategory','FoodCategoryController@index')->name('foodcategory');
    Route::get('foodcategory/add','FoodCategoryController@add')->name('foodcategory.add');
    Route::post('foodcategory/save','FoodCategoryController@save')->name('foodcategory.save');
    Route::get('foodcategory/edit/{id}','FoodCategoryController@edit')->name('foodcategory.edit');
    Route::post('foodcategory/update/{id}','FoodCategoryController@update')->name('foodcategory.update');
    Route::get('foodcategory/delete/{id}','FoodCategoryController@delete')->name('foodcategory.delete');

    //Routes For Food Category Table
    Route::get('foodsubcategory','FoodSubCategoryController@index')->name('foodsubcategory');
    Route::get('foodsubcategory/add','FoodSubCategoryController@add')->name('foodsubcategory.add');
    Route::post('foodsubcategory/save','FoodSubCategoryController@save')->name('foodsubcategory.save');
    Route::get('foodsubcategory/edit/{id}','FoodSubCategoryController@edit')->name('foodsubcategory.edit');
    Route::post('foodsubcategory/update/{id}','FoodSubCategoryController@update')->name('foodsubcategory.update');
    Route::get('foodsubcategory/delete/{id}','FoodSubCategoryController@delete')->name('foodsubcategory.delete');

    //Routes For Food Table
    Route::get('food','FoodController@index')->name('food');
    Route::get('food/add','FoodController@add')->name('food.add');
    Route::post('food/save','FoodController@save')->name('food.save');
    Route::get('food/edit/{id}','FoodController@edit')->name('food.edit');
    Route::post('food/update/{id}','FoodController@update')->name('food.update');
    Route::get('food/delete/{id}','FoodController@delete')->name('food.delete');

    //Routes For Food Type table
    Route::get('foodtype','FoodTypeController@index')->name('foodtype');
    Route::get('foodtype/add','FoodTypeController@add')->name('foodtype.add');
    Route::post('foodtype/save','FoodTypeController@save')->name('foodtype.save');
    Route::get('foodtype/edit/{id}','FoodTypeController@edit')->name('foodtype.edit');
    Route::post('foodtype/update/{id}','FoodTypeController@update')->name('foodtype.update');
    Route::get('foodtype/delete/{id}','FoodTypeController@delete')->name('foodtype.delete');

    //Routes For Food Favourate table
    Route::get('foodfavourate','FoodFavourateController@index')->name('foodfavourate');
    Route::get('foodfavourate/add','FoodFavourateController@add')->name('foodfavourate.add');
    Route::post('foodfavourate/save','FoodFavourateController@save')->name('foodfavourate.save');
    Route::get('foodfavourate/edit/{id}','FoodFavourateController@edit')->name('foodfavourate.edit');
    Route::post('foodfavourate/update/{id}','FoodFavourateController@update')->name('foodfavourate.update');
    Route::get('foodfavourate/delete/{id}','FoodFavourateController@delete')->name('foodfavourate.delete');

    //Routes For Opening Hour table
    Route::get('openinghour','OpeningHourController@index')->name('openinghour');
    Route::get('openinghour/add','OpeningHourController@add')->name('openinghour.add');
    Route::post('openinghour/save','OpeningHourController@save')->name('openinghour.save');
    Route::get('openinghour/edit/{id}','OpeningHourController@edit')->name('openinghour.edit');
    Route::post('openinghour/update/{id}','OpeningHourController@update')->name('openinghour.update');
    Route::get('openinghour/delete/{id}','OpeningHourController@delete')->name('openinghour.delete');

    //Routes For Menu table
    Route::get('menu','MenuController@index')->name('menu');
    Route::get('menu/add','MenuController@add')->name('menu.add');
    Route::post('menu/save','MenuController@save')->name('menu.save');
    Route::get('menu/edit/{id}','MenuController@edit')->name('menu.edit');
    Route::post('menu/update/{id}','MenuController@update')->name('menu.update');
    Route::get('menu/delete/{id}','MenuController@delete')->name('menu.delete');

    //Routes For User table
    Route::get('user','UserController@index')->name('user');
    Route::get('user/add','UserController@add')->name('user.add');
    Route::post('user/save','UserController@save')->name('user.save');
    Route::get('user/edit/{id}','UserController@edit')->name('user.edit');
    Route::post('user/update/{id}','UserController@update')->name('user.update');
    Route::get('user/delete/{id}','UserController@delete')->name('user.delete');

        Route::get('home','FrontPageController@index')->name('front');
    Route::get('search','SearchController@index')->name('search');
    Route::get('detail/{id}','DetailController@index')->name('detail');
    Route::get('addtocart/{id}/{name}/{price}','DetailController@addtocart')->name('addtocart');


});
