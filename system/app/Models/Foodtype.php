<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foodtype extends Model
{
    protected $fillable=['name','status'];
}
