<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fliptext extends Model
{
    protected $fillable=['text','status'];
}
