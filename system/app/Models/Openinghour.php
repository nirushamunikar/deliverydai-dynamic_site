<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Openinghour extends Model
{
    protected $fillable=['restaurant_id','day','open_time','close_time','status'];
}
