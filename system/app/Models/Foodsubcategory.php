<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foodsubcategory extends Model
{
    protected $fillable=['name'];
}
