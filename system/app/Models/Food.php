<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable=['name','food_type_id','description','food_category_id','food_subcategory_id','fav_count','recommended'];
}
