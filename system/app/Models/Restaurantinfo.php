<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurantinfo extends Model
{
    protected $fillable=[
        'name','address','latitude','longitude','logo','bgimage','restaurant_type','custom_field_name',
        'custom_field_value','delivery_time','service_charge','vat','restaurant_category','offer_title',
        'offer_value','offer_type','recommended','show_in_home'
    ];
}
