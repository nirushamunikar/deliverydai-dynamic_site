<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foodfavourate extends Model
{
    protected $fillable=['food_id','customer_id','status'];
}
