<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Homepanel extends Model
{

    protected $fillable=['logo','caption','bgimage','videourl','twitterlink','fblink','instalink','googlelink','status'];
}
