<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable=['restaurant_id','food_id','photo','unit','sale_price','offer_price','offer','recommended'];
}
