<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurantinfo;
use Illuminate\Support\Facades\Storage;

class RestaurantInfoController extends Controller
{
    private $restaurantinfo;

    public function __construct(Restaurantinfo $restaurantinfo)
    {
        $this->restaurantinfo=$restaurantinfo;
    }

    public function index()
    {
        $data=$this->restaurantinfo->all();

        return view('restaurantinfo.restaurantinfo')->with(compact('data'));
    }
    public function add()
    {
        return view('restaurantinfo.addrestaurantinfo');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'restaurant_type'=>$request->input('restaurant_type'),
            'custom_field_name'=>$request->input('custom_field_name'),
            'custom_field_value'=>$request->input('custom_field_value'),
            'delivery_time'=>$request->input('delivery_time'),
            'service_charge'=>$request->input('service_charge'),
            'vat'=>$request->input('vat'),
            'restaurant_category'=>json_encode($request->input('restaurant_category')),
            'offer_title'=>$request->input('offer_title'),
            'offer_value'=>$request->input('offer_value'),
            'offer_type'=>$request->input('offer_type'),
            'recommended'=>$request->input('recommended')
        ];
        if($request->hasFile('logo'))
        {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/logo/';
            $request->file('logo')->move($destination,$filename);
            $data['logo']=$filename;
        }

        if($request->hasFile('bgimage'))
        {
            $extension = $request->file('bgimage')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/background';
            $request->file('bgimage')->move($destination,$filename);
            $data['bgimage']=$filename;
        }

        $this->restaurantinfo->create($data);

        return redirect('restaurantinfo');
    }

    public function edit($id)
    {
        $data= $this->restaurantinfo->find($id);

        return view('restaurantinfo.updaterestaurantinfo')->with(compact('data'));
    }

    public function update($id,Request $request)
    {
        $old=$this->restaurantinfo->find($id);

        $data=[
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'restaurant_type'=>$request->input('restaurant_type'),
            'custom_field_name'=>$request->input('custom_field_name'),
            'custom_field_value'=>$request->input('custom_field_value'),
            'delivery_time'=>$request->input('delivery_time'),
            'service_charge'=>$request->input('service_charge'),
            'vat'=>$request->input('vat'),
            'restaurant_category'=>json_encode($request->input('restaurant_category')),
            'offer_title'=>$request->input('offer_title'),
            'offer_value'=>$request->input('offer_value'),
            'offer_type'=>$request->input('offer_type'),
            'recommended'=>$request->input('recommended')
        ];


        if($request->hasFile('logo'))
        {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/logo/';
            $request->file('logo')->move($destination,$filename);

            $p = $this->restaurantinfo->find($id);

            if ($p->logo)
                Storage::delete('assets/images/restaurant/logo/'.$p->logo);

            $data['logo'] = $filename;
        }

        if($request->hasFile('bgimage'))
        {
            $extension = $request->file('bgimage')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/background';
            $request->file('bgimage')->move($destination,$filename);

            $p = $this->restaurantinfo->find($id);

            if ($p->bgimage)
                Storage::delete('assets/images/restaurant/background'.$p->bgimage);

            $data['bgimage'] = $filename;


        }

        $old->update($data);

        return redirect('restaurantinfo');
    }
    public function delete($id)
    {
        $old=$this->restaurantinfo->find($id);
        $old->delete();
        return redirect('restaurantinfo');
    }
}
