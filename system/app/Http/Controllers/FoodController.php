<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Foodcategory;
use App\Models\Foodtype;
use Illuminate\Http\Request;
use DB;

class FoodController extends Controller
{
    private $food;
    private $foodcat;
    private $foodtype;
    public function __construct(Food $food,Foodcategory $foodcat,Foodtype $foodtype)
    {
        $this->food=$food;
        $this->foodcat=$foodcat;
        $this->foodtype=$foodtype;
    }

    public function index()
    {
        //$data=$this->food->all();
        $data= DB::table('foods')
                      ->join('foodcategories','foods.food_category_id','=','foodcategories.id')
                      ->join('foodsubcategories','foods.food_subcategory_id','=','foodsubcategories.id')
                      ->join('foodtypes','foods.food_type_id','=','foodtypes.id')
                      ->select('foods.id','foods.name','foodcategories.name as food_category_id','foodsubcategories.name as food_subcategory_id',
                                'foodtypes.name as food_type_id','foods.description','foods.fav_count','foods.recommended')
                      ->get();

        return view('food.food')->with(compact('data'));
    }
    public function add()
    {
        $foodcats = DB::table('foodcategories')->select('id','name')->get();
        $foodsubcats = DB::table('foodsubcategories')->select('id','name')->get();
        $foodtype = DB::table('foodtypes')->select('id','name')->get();

        return view('food.addfood')->with(compact('foodcats','foodtype','foodsubcats'));
    }

    public function save(Request $request)
    {
        $data=[
            'food_category_id'=>$request->input('food_category_id'),
            'food_subcategory_id'=>$request->input('food_subcategory_id'),
            'name'=>$request->input('name'),
            'food_type_id'=>$request->input('food_type_id'),
            'description'=>$request->input('description'),
            'fav_count'=>$request->input('fav_count'),
            'recommended'=>$request->input('recommended')
        ];
        $this->food->create($data);
        return redirect('food');
    }

    public function edit($id)
    {
        $data= $this->food->find($id);
        $foodcats = DB::table('foodcategories')->select('id','name')->get();
        $foodsubcats = DB::table('foodsubcategories')->select('id','name')->get();
        $foodtype = DB::table('foodtypes')->select('id','name')->get();
        return view('food.updatefood')->with(compact('data','foodcats','foodtype','foodsubcats'));
    }

    public function update($id,Request$request)
    {
        $old=$this->food->find($id);

        $data=[
            'food_category_id'=>$request->input('food_category_id'),
            'food_subcategory_id'=>$request->input('food_subcategory_id'),
            'name'=>$request->input('name'),
            'food_type_id'=>$request->input('food_type_id'),
            'description'=>$request->input('description'),
            'fav_count'=>$request->input('fav_count'),
            'recommended'=>$request->input('recommended')
        ];
        $old->update($data);

        return redirect('food');
    }
    public function delete($id)
    {
        $old=$this->food->find($id);
        $old->delete();
        return redirect('food');
    }
}
