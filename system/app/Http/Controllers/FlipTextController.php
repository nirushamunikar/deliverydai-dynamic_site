<?php

namespace App\Http\Controllers;

use App\Models\Fliptext;
use Illuminate\Http\Request;

class FlipTextController extends Controller
{
    private $fliptext;

    public function __construct(Fliptext $fliptext)
    {
        $this->fliptext=$fliptext;
    }

    public function index()
    {
        $data=$this->fliptext->all();

        return view('fliptext.fliptext')->with(compact('data'));
    }
    public function add()
    {
        return view('fliptext.addfliptext');
    }

    public function save(Request $request)
    {
        $data=[
            'text'=>$request->input('text'),
            'status'=>$request->input('status')
        ];
        $this->fliptext->create($data);
        return redirect('fliptext');
    }

    public function edit($id)
    {
        $data= $this->fliptext->find($id);

        return view('fliptext.updatefliptext')->with(compact('data'));
    }

    public function update($id,Request$request)
    {
        $old=$this->fliptext->find($id);

        $data=[
            'text'=>$request->input('text'),
            'status'=>$request->input('status')
        ];
        $old->update($data);

        return redirect('fliptext');
    }
    public function delete($id)
    {
        $old=$this->fliptext->find($id);
        $old->delete();
        return redirect('fliptext');
    }

}
