<?php

namespace App\Http\Controllers;

use App\Models\Openinghour;
use App\Models\Restaurantinfo;
use Illuminate\Http\Request;
use DB;

class OpeningHourController extends Controller
{
    private $openinghour;
    private $restinfo;

    public function __construct(Openinghour $openinghour,Restaurantinfo $restinfo)
    {
        $this->openinghour=$openinghour;
        $this->restinfo=$restinfo;
    }

    public function index()
    {
        //$data=$this->openinghour->all();
        $data=DB::table('openinghours')
                ->join('restaurantinfos','openinghours.restaurant_id','=','restaurantinfos.id')
                ->select('openinghours.id','restaurantinfos.name as restaurant_id','openinghours.day','openinghours.open_time',
                    'openinghours.close_time','openinghours.status')
                ->get();

        return view('openinghour.openinghour')->with(compact('data'));
    }
    public function add()
    {
        $restinfo=DB::table('restaurantinfos')->select('id','name')->get();
        return view('openinghour.addopeninghour')->with(compact('restinfo'));
    }

    public function save(Request $request)
    {
        $data=[
            'restaurant_id'=>$request->input('restaurant_id'),
            'day'=>$request->input('day'),
            'open_time'=>$request->input('open_time'),
            'close_time'=>$request->input('close_time'),
            'status'=>$request->input('status')
        ];
        $this->openinghour->create($data);
        return redirect('openinghour');
    }

    public function edit($id)
    {
        $data= $this->openinghour->find($id);
        $restinfo=DB::table('restaurantinfos')->select('id','name')->get();
        return view('openinghour.updateopeninghour')->with(compact('data','restinfo'));
    }

    public function update($id,Request$request)
    {
        $old=$this->openinghour->find($id);

        $data=[
            'restaurant_id'=>$request->input('restaurant_id'),
            'day'=>$request->input('day'),
            'open_time'=>$request->input('open_time'),
            'close_time'=>$request->input('close_time'),
            'status'=>$request->input('status')
        ];
        $old->update($data);

        return redirect('openinghour');
    }
    public function delete($id)
    {
        $old=$this->openinghour->find($id);
        $old->delete();
        return redirect('openinghour');
    }
}
