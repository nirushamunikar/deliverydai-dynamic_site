<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Foodcategory;
use App\Models\Foodsubcategory;
use App\Models\Menu;
use App\Models\Restaurantinfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    private $menu;
    private $restinfo;
    private $foodcat;
    private $foodsubcat;
    private $food;


    public function __construct(Menu $menu,Restaurantinfo $restinfo,Foodcategory $foodcat,Food $food,Foodsubcategory $foodsubcat)
    {
        $this->menu=$menu;
        $this->restinfo=$restinfo;
        $this->foodcat=$foodcat;
        $this->foodsubcat=$foodsubcat;
        $this->food=$food;
    }

    public function index()
    {
        //$data=$this->menu->all();

        $data=DB::table('menus')
                    ->join('restaurantinfos','menus.restaurant_id','=','restaurantinfos.id')
                    ->join('foods','menus.food_id','=','foods.id')
                    ->select('menus.id','restaurantinfos.name as restaurant_id','foods.name as food_id','menus.photo','menus.unit','menus.sale_price','menus.offer_price','menus.offer','menus.recommended')
                    ->get();

        return view('menu.menu')->with(compact('data'));
    }
    public function add()
    {
        $restinfo = DB::table('restaurantinfos')->select('id','name')->get();
        $foodcat = DB::table('foodcategories')->select('id','name')->get();
        $foodsubcat = DB::table('foodsubcategories')->select('id','name')->get();
        $foods = DB::table('foods')->select('id','name')->get();
        return view('menu.addmenu')->with(compact('restinfo','foodcat','foods','foodsubcat'));
    }

    public function save(Request $request)
    {
        $data=[
            'restaurant_id'=>$request->input('restaurant_id'),
            'food_id'=>$request->input('food_id'),
            'unit'=>$request->input('unit'),
            'sale_price'=>$request->input('sale_price'),
            'offer_price'=>$request->input('offer_price'),
            'offer'=>$request->input('offer'),
            'recommended'=>$request->input('recommended')
        ];
        if($request->hasFile('photo'))
        {
            $extension = $request->file('photo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/menu/';
            $request->file('photo')->move($destination,$filename);
            $data['photo']=$filename;
        }
        $this->menu->create($data);
        return redirect('menu');
    }

    public function edit($id)
    {
        $restinfo = DB::table('restaurantinfos')->select('id','name')->get();
        $foodcat = DB::table('foodcategories')->select('id','name')->get();
        $foodsubcat = DB::table('foodsubcategories')->select('id','name')->get();
        $foods = DB::table('foods')->select('id','name')->get();
        $data= $this->menu->find($id);

        return view('menu.updatemenu')->with(compact('data','restinfo','foodcat','foods','foodsubcat'));
    }

    public function update($id,Request$request)
    {
        $old=$this->menu->find($id);

        $data=[
            'restaurant_id'=>$request->input('restaurant_id'),
            'food_id'=>$request->input('food_id'),
            'unit'=>$request->input('unit'),
            'sale_price'=>$request->input('sale_price'),
            'offer_price'=>$request->input('offer_price'),
            'offer'=>$request->input('offer'),
            'recommended'=>$request->input('recommended')
        ];
        if($request->hasFile('photo'))
        {
            $extension = $request->file('photo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/restaurant/menu/';
            $request->file('photo')->move($destination,$filename);

            $p = $this->menu->find($id);

            if ($p->photo)
                Storage::delete('assets/images/restaurant/menu/'.$p->photo);

            $data['photo'] = $filename;
        }
        $old->update($data);

        return redirect('menu');
    }
    public function delete($id)
    {
        $old=$this->menu->find($id);
        $old->delete();
        return redirect('menu');
    }
}
