<?php

namespace App\Http\Controllers;

use App\Models\Foodsubcategory;
use Illuminate\Http\Request;
use DB;

class FoodSubCategoryController extends Controller
{
    private $foodsubcategory;

    public function __construct(Foodsubcategory $foodsubcategory)
    {
        $this->foodsubcategory=$foodsubcategory;
    }

    public function index()
    {
        //$data=$this->foodsubcategory->all();
        $data= DB::table('foodsubcategories')
                            ->select('foodsubcategories.id','foodsubcategories.name'    )
                            ->get();

        return view('foodsubcategory.foodsubcategory')->with(compact('data'));
    }
    public function add()
    {
        //$foodcats = DB::table('foodcategories')->select('id','name')->get();

        return view('foodsubcategory.addfoodsubcategory');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
        ];
        $this->foodsubcategory->create($data);
        return redirect('foodsubcategory');
    }

    public function edit($id)
    {
        $data= $this->foodsubcategory->find($id);
       // $foodcats = DB::table('foodcategories')->select('id','name')->get();

        return view('foodsubcategory.updatefoodsubcategory')->with(compact('data'));
    }

    public function update($id,Request$request)
    {
        $old=$this->foodsubcategory->find($id);

        $data=[
            'name'=>$request->input('name'),
        ];
        $old->update($data);

        return redirect('foodsubcategory');
    }
    public function delete($id)
    {
        $old=$this->foodsubcategory->find($id);
        $old->delete();
        return redirect('foodsubcategory');
    }
}
