<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Foodfavourate;
use Illuminate\Http\Request;
use DB;

class FoodFavourateController extends Controller
{
    private $foodfavourate;
    private $food;

    public function __construct(Foodfavourate $foodfavourate,Food $food)
    {
        $this->foodfavourate=$foodfavourate;
        $this->food=$food;
    }

    public function index()
    {
        //$data=$this->foodfavourate->all();
        $data=DB::table('foodfavourates')
                    ->join('foods','foodfavourates.food_id','=','foods.id')
                    ->select('foodfavourates.id','foods.name as food_id','foodfavourates.customer_id','foodfavourates.status')
                    ->get();
        return view('foodfavourate.foodfavourate')->with(compact('data'));
    }
    public function add()
    {
        $foods = DB::table('foods')->select('id','name')->get();
        return view('foodfavourate.addfoodfavourate')->with(compact('foods'));
    }

    public function save(Request $request)
    {
        $data=[
            'food_id'=>$request->input('food_id'),
            'customer_id'=>$request->input('customer_id'),
            'status'=>$request->input('status')
        ];
        $this->foodfavourate->create($data);
        return redirect('foodfavourate');
    }

    public function edit($id)
    {
        $data= $this->foodfavourate->find($id);
        $foods = DB::table('foods')->select('id','name')->get();
        return view('foodfavourate.updatefoodfavourate')->with(compact('data','foods'));
    }

    public function update($id,Request$request)
    {
        $old=$this->foodfavourate->find($id);

        $data=[
            'food_id'=>$request->input('food_id'),
            'customer_id'=>$request->input('customer_id'),
            'status'=>$request->input('status')
        ];
        $old->update($data);

        return redirect('foodfavourate');
    }
    public function delete($id)
    {
        $old=$this->foodfavourate->find($id);
        $old->delete();
        return redirect('foodfavourate');
    }
}
