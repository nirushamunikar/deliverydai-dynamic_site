<?php

namespace App\Http\Controllers;

use App\Models\Foodcategory;
use Illuminate\Http\Request;

class FoodCategoryController extends Controller
{
    private $foodcategory;

    public function __construct(Foodcategory $foodcategory)
    {
        $this->foodcategory=$foodcategory;
    }

    public function index()
    {
        $data=$this->foodcategory->all();

        return view('foodcategory.foodcategory')->with(compact('data'));
    }
    public function add()
    {
        return view('foodcategory.addfoodcategory');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'status'=>$request->input('status')
        ];
        $this->foodcategory->create($data);
        return redirect('foodcategory');
    }

    public function edit($id)
    {
        $data= $this->foodcategory->find($id);

        return view('foodcategory.updatefoodcategory')->with(compact('data'));
    }

    public function update($id,Request$request)
    {
        $old=$this->foodcategory->find($id);

        $data=[
            'name'=>$request->input('name'),
            'status'=>$request->input('status')
        ];
        $old->update($data);

        return redirect('foodcategory');
    }
    public function delete($id)
    {
        $old=$this->foodcategory->find($id);
        $old->delete();
        return redirect('foodcategory');
    }
}
