<?php

namespace App\Http\Controllers;

use App\Models\Homepanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomePanelController extends Controller
{
    private $homepanel;

    public function __construct(Homepanel $homepanel)
    {
        $this->homepanel=$homepanel;
    }

    public function index()
    {
        $data=$this->homepanel->all();

        return view('homepanel.homepanel')->with(compact('data'));
    }

    public function add()
    {
        return view('homepanel.addhomepanel');
    }

    public function save(Request $request)
    {
        $data=[
            'caption'=>$request->input('caption'),
            'videourl'=>$request->input('videourl'),
            'twitterlink'=>$request->input('twitterlink'),
            'fblink'=>$request->input('fblink'),
            'instalink'=>$request->input('instalink'),
            'googlelink'=>$request->input('googlelink'),
            'status'=>$request->input('status')
        ];
        if($request->hasFile('logo'))
        {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/homepanel/logo/';
            $request->file('logo')->move($destination,$filename);
            $data['logo']=$filename;
        }

        if($request->hasFile('bgimage'))
        {
            $extension = $request->file('bgimage')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/homepanel/background/';
            $request->file('bgimage')->move($destination,$filename);
            $data['bgimage']=$filename;
        }

        $this->homepanel->create($data);

        return redirect('homepanel');
    }

    public function edit($id)
    {
        $data = $this->homepanel->find($id);

        return view('homepanel.updatehomepanel')->with(compact('data'));
    }

    public function update(Request $request,$id)
    {
        $old= $this->homepanel->find($id);

        $data=[
            'caption'=>$request->input('caption'),
            'videourl'=>$request->input('videourl'),
            'twitterlink'=>$request->input('twitterlink'),
            'fblink'=>$request->input('fblink'),
            'instalink'=>$request->input('instalink'),
            'googlelink'=>$request->input('googlelink'),
            'status'=>$request->input('status')
        ];

        if($request->hasFile('logo'))
        {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/homepanel/logo/';
            $request->file('logo')->move($destination,$filename);

            $p = $this->homepanel->find($id);


            if ($p->logo)
                Storage::delete('assets/images/homepanel/logo/'.$p->logo);


            $data['logo'] = $filename;
        }

        if($request->hasFile('bgimage'))
        {
            $extension = $request->file('bgimage')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/homepanel/bavkground/';
            $request->file('bgimage')->move($destination,$filename);

            $p = $this->homepanel->find($id);

            if ($p->logo)
                Storage::delete('assets/images/homepanel/background/'.$p->logo);


            $data['bgimage'] = $filename;
        }
        $old->update($data);

        return redirect('homepanel');
    }

    public function delete($id)
    {
        $old=$this->homepanel->find($id);

        $old->delete();

        return redirect('homepanel');
    }
}
