<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
       //validation

       $data = [
            'email'=> $request->input('email'),
             'password'=> bcrypt($request->input('password'))
           ];


       if(Auth::attempt(['email'=>$request->input('email'),'password'=>$request->input('password')],$request->input('remember'))){
            return redirect()->intended('restaurantinfo');
       };

    }
    public function logout(){
        Auth::logout();
        return redirect()->intended('login');
    }

    public function control()
    {
        return view('dashboard');
    }
}
