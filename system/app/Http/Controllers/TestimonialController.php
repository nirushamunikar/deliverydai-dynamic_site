<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends Controller
{
    private $testimonial;

    public function __construct(Testimonial $testimonial)
    {
        $this->testimonial=$testimonial;
    }

    public function index()
    {
        $data = $this->testimonial->all();

        return view('testimonial.testimonial')->with(compact('data'));
    }

    public function add()
    {
        return view('testimonial.addtestimonial');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'review'=>$request->input('review'),
            'company_name'=>$request->input('company_name'),
            'position'=>$request->input('position'),
            'status'=>$request->input('status')
        ];

        if($request->hasFile('photo'))
        {
            $extension = $request->file('photo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/testimonial/';
            $request->file('photo')->move($destination,$filename);
            $data['photo']=$filename;
        }

        $this->testimonial->create($data);

        return redirect('testimonial');
    }

    public function edit($id)
    {
        $data= $this->testimonial->find($id);

        return view('testimonial.updatetestimonial')->with(compact('data'));
    }

    public function update($id,Request $request)
    {
        $old=$this->testimonial->find($id);

        $data=[
            'name'=>$request->input('name'),
            'review'=>$request->input('review'),
            'company_name'=>$request->input('company_name'),
            'position'=>$request->input('position'),
            'status'=>$request->input('status')
        ];


        if($request->hasFile('photo'))
        {
            $extension = $request->file('photo')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/testimonial/';
            $request->file('photo')->move($destination,$filename);

            $p = $this->testimonial->find($id);

            if ($p->photo)
                Storage::delete('assets/images/testimonial/'.$p->photo);

            $data['photo'] = $filename;


        }


        $old->update($data);

        return redirect('testimonial');
    }
    public function delete($id)
    {
        $old=$this->testimonial->find($id);
        $old->delete();
        return redirect('testimonial');
    }

}
