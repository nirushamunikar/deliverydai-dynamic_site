<?php

namespace App\Http\Controllers;

use App\Models\Foodcategory;
use App\Models\Foodsubcategory;
use App\Models\Menu;
use App\Models\Restaurantinfo;
use Illuminate\Http\Request;
use DB;

class DetailController extends Controller
{
    private $restinfo;
    private $foodcat;
    private $foodsubcat;
    private $menu;
    public function __construct(Restaurantinfo $restinfo,Foodcategory $foodcat,Foodsubcategory $foodsubcat,Menu $menu)
    {
        $this->restinfo=$restinfo;
        $this->foodcat=$foodcat;
        $this->foodsubcat=$foodsubcat;
        $this->menu=$menu;
    }

    public function index($id)
    {

        $info=$this->restinfo->find($id);

        $foodcat=DB::table('menus')
                    ->join('foods','menus.food_id','=','foods.id')
                    ->where('menus.restaurant_id','=',$id)
                    ->select('foods.food_category_id')
                    ->distinct()
                    ->get();

//        $foodscat=DB::table('menus')
//                ->join('foods','menus.food_id','=','foods.id')
//                ->where('menus.restaurant_id','=',$id)
//                ->select('foods.food_subcategory_id')
//                ->get();



        $foodcategory=[];
        foreach ($foodcat as $parent)
        {
            $catid = $parent->food_category_id;
            //$scatid = $parent->food_subcategory_id;

            $foo =[
                   'Parent'=> DB::table('foodcategories')
                                ->select('id','name')
                                ->where('id','=',$catid)
                                ->get(),
                    'Sub'=> DB::table('foodsubcategories')
                        ->join('foods','foodsubcategories.id','=','foods.food_subcategory_id')
                        ->select('foodsubcategories.id','foodsubcategories.name')
                        ->where('foods.food_category_id','=',$catid)
                        ->get(),
            ];
            array_push($foodcategory, $foo);
        }

        $recs =DB::table('menus')
                ->join('foods','menus.food_id','=','foods.id')
                ->where('menus.recommended','=','1')
                ->where('menus.restaurant_id','=',$id)
                ->select('menus.id','foods.name','foods.description','menus.photo','menus.sale_price','menus.offer_price','menus.unit','menus.offer')
                ->get();


        $menus=DB::table('menus')
                ->join('foods','menus.food_id','=','foods.id')
                ->where('menus.restaurant_id','=',$id)
                ->select('foods.id','foods.name','foods.food_category_id','foods.food_subcategory_id','foods.description','menus.photo','menus.sale_price','menus.offer_price','menus.unit','menus.offer')
                ->get();

        $carts =DB::table('menus')
                ->join('foods','menus.food_id','=','foods.id')
                ->join('restaurantinfos','menus.restaurant_id','=','restaurantinfos.id')
                ->where('menus.restaurant_id','=',$id)
                ->select('foods.id','foods.name','menus.sale_price','restaurantinfos.vat')
                ->get();

        return view('site.detail')->with(compact('info','foodcategory','recs','menus','carts'));
    }

    public function addtocart($id,$price,$name)
    {
        if(empty($_SESSION['cart'])){
            $_SESSION['cart'] = array();
            $cart=['product_id'=>$id,'Product_name'=>$name,'Price'=>$price];
            array_push($_SESSION['cart'],$cart);
            foreach ($_SESSION['cart'] as $product)
            {
                dd($product);
            }
        }



    }
}
