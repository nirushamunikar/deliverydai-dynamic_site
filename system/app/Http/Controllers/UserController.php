<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user=$user;
    }

    public function index()
    {
        $data=$this->user->all();

        return view('user.user')->with(compact('data'));
    }
    public function add()
    {
        return view('user.adduser');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'address'=>$request->input('address'),
            'email'=>$request->input('email'),
            'password'=>$request->input('password'),
            'social_user_id'=>$request->input('social_user_id'),
            'social_access_token'=>$request->input('social_access_token'),
            'last_login'=>$request->input('last_login'),
            'permissions'=>$request->input('permissions'),
            'ip_address'=>$request->input('ip_address'),
            'role'=>$request->input('role')
        ];
        if($request->hasFile('image'))
        {
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/user/';
            $request->file('image')->move($destination,$filename);
            $data['image']=$filename;
        }
        $this->user->create($data);
        return redirect('user');
    }

    public function edit($id)
    {
        $data= $this->user->find($id);

        return view('user.updateuser')->with(compact('data'));
    }

    public function update($id,Request$request)
    {
        $old=$this->user->find($id);

        $data=[
            'name'=>$request->input('name'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'address'=>$request->input('address'),
            'email'=>$request->input('email'),
            'password'=>$request->input('password'),
            'social_user_id'=>$request->input('social_user_id'),
            'social_access_token'=>$request->input('social_access_token'),
            'last_login'=>$request->input('last_login'),
            'permissions'=>$request->input('permissions'),
            'ip_address'=>$request->input('ip_address'),
            'role'=>$request->input('role')
        ];
        if($request->hasFile('image'))
        {
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename  = str_random(19).'.'.$extension;
            $destination = 'assets/images/user/';
            $request->file('image')->move($destination,$filename);

            $p = $this->user->find($id);

            if ($p->logo)
                Storage::delete('assets/images/user/'.$p->logo);

            $data['image'] = $filename;
        }

        $old->update($data);

        return redirect('user');
    }

    public function delete($id)
    {
        $old=$this->user->find($id);
        $old->delete();
        return redirect('user');
    }
}
