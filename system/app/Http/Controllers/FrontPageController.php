<?php

namespace App\Http\Controllers;

use App\Models\Fliptext;
use App\Models\Food;
use App\Models\Homepanel;
use App\Models\Menu;
use App\Models\Restaurantinfo;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use DB;

class FrontPageController extends Controller
{
    private $testimonial;
    private $homepanel;
    private $fliptext;
    private $resinfo;
    private $menu;
    private $food;
    public function __construct(Testimonial $testimonial,Homepanel $homepanel,Fliptext $fliptext,Restaurantinfo $resinfo,
                                Menu $menu,Food $food)
    {
        $this->testimonial=$testimonial;
        $this->homepanel=$homepanel;
        $this->fliptext=$fliptext;
        $this->resinfo=$resinfo;
        $this->menu=$menu;
        $this->food=$food;
    }

    public function index()
    {
        $testimonials =$this->testimonial->all();
        $homepanels =$this->homepanel->all();
        $fliptexts =$this->fliptext->all();
        $resinfos =$this->resinfo->all();
        $menus=DB::table('foods')
                    ->join('menus','foods.id','=','menus.food_id')
                    ->select('foods.id','foods.name','menus.sale_price','menus.unit','menus.photo','foods.fav_count')
                    ->orderBy('foods.fav_count','DESC')->limit(6)->get();

        return view('site.index')->with(compact('testimonials','homepanels','fliptexts','resinfos','menus'));
    }
}
