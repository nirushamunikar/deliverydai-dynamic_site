<?php

namespace App\Http\Controllers;

use App\Models\Foodtype;
use Illuminate\Http\Request;

class FoodTypeController extends Controller
{
    private $foodtype;

    public function __construct(Foodtype $foodtype)
    {
        $this->foodtype=$foodtype;
    }

    public function index()
    {
        $data=$this->foodtype->all();

        return view('foodtype.foodtype')->with(compact('data'));
    }
    public function add()
    {
        return view('foodtype.addfoodtype');
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'status'=>$request->input('status')
        ];
        $this->foodtype->create($data);
        return redirect('foodtype');
    }

    public function edit($id)
    {
        $data= $this->foodtype->find($id);

        return view('foodtype.updatefoodtype')->with(compact('data'));
    }

    public function update($id,Request$request)
    {
        $old=$this->foodtype->find($id);

        $data=[
            'name'=>$request->input('name'),
            'status'=>$request->input('status')
        ];
        $old->update($data);

        return redirect('foodtype');
    }
    public function delete($id)
    {
        $old=$this->foodtype->find($id);
        $old->delete();
        return redirect('foodtype');
    }
}
