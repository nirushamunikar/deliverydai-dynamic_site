<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    private $menu;
    public function __construct(Menu $menu)
    {
        $this->menu=$menu;
    }

    public function index()
    {
        $picks=DB::table('menus')
                    ->join('restaurantinfos','menus.restaurant_id','=','restaurantinfos.id')
                    ->select('menus.id','menus.photo','menus.sale_price','menus.unit','menus.restaurant_id',
                            'restaurantinfos.restaurant_category','restaurantinfos.delivery_time','restaurantinfos.name',
                            'restaurantinfos.bgimage','restaurantinfos.restaurant_type','restaurantinfos.custom_field_value as customvalue',
                            'restaurantinfos.custom_field_name as customname')
                    ->limit(6)
                    ->get();
        return view('site.search')->with(compact('picks'));
    }
}
