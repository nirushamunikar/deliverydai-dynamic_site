$(document).ready(function(){
    //scroll
    $(".header-banner .scroll").on("click",function(){
        $("html,body").animate({
            scrollTop:$(".header-banner").outerHeight()
        },500);
    });
   
    // slick slider
    $(".section-testimonials .content").slick({
        arrows:false,
        dots:true,
        adaptiveHeight:true,
        autoplay:true,
        autoplaySpeed:1500,
    });
    if($(window).width() < 768){
        $(".favorite-row").slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow:"<i class='fa fa-angle-left slick-arrow-left'></i>",
            nextArrow:"<i class='fa fa-angle-right slick-arrow-right'></i>",
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                    }
                }
            ]
        });
    }

    //slide Item Wrapper
    $(".slide-item-wrapper").slick({
        arrows:false,
        dots:false,
        adaptiveHeight:true,
        autoplay:true,
        autoplaySpeed:1500,
        slidesToShow: 3,
        slidesToScroll: 1
    });

    // sticky header
    $(".sticky-navbar").sticky({topSpacing:0});
    $('.sticky-navbar').on('sticky-start', function() {
        $(".sticky-navbar").addClass("sticked");
    });
    $('.sticky-navbar').on('sticky-end', function() { 
        $(".sticky-navbar").removeClass("sticked");
    });

    //tweenmax
    		
    var $slogans = $("p.slogan").find("strong");
    var $holder = $("#holder");
    
    //set via JS so they're visible if JS disabled
    $slogans.parent().css({position : "absolute", top:"0px", left:"0px"});
    
    //settings
    var transitionTime = 0.4;
    var slogansDelayTime = 5;
    
    //internal
    var totalSlogans = $slogans.length;
    
    var oldSlogan = 0;
    var currentSlogan = -1;
    
    //initialize	
    switchSlogan();
    
    function switchSlogan(){
        
        oldSlogan = currentSlogan;
        
        if(currentSlogan < totalSlogans-1){
            currentSlogan ++
        } else {
            currentSlogan = 0;
        }
        
        TweenLite.to($slogans.eq(oldSlogan), transitionTime, {top:-20, alpha:0, rotationX: 90});
        TweenLite.fromTo($slogans.eq(currentSlogan), transitionTime, {top:20, alpha:0, rotationX: -90 }, {top:0, alpha:1, rotationX:0});
        
        TweenLite.delayedCall(slogansDelayTime, switchSlogan);
    }   


    //sticky js
    $("#sticky-sidebar").stick_in_parent({
        offset_top:100
    });


    // Cache selectors
    var lastId,
    topMenu = $(".restaurant-content-sidebar"),
    topMenuHeight = $(".navbar").outerHeight() - 15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
    var item = $($(this).attr("href"));
    if (item.length) { return item; }
    });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
    var href = $(this).attr("href"),
    offsetTop= $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
    scrollTop: offsetTop
    }, 300);
    e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function(){
    if ($(this).offset().top < fromTop)
    return this;
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : "";

    if (lastId !== id) {
    lastId = id;
    // Set/remove active class
    menuItems
        .parent().removeClass("active")
        .end().filter("[href='#"+id+"']").parent().addClass("active");
    }                   
    });


    // modal-show
    $("[data-modal]").on("click",function(){
        var target=$(this).attr('data-modal');
        $(target).addClass('custom-modal-show');
        return false;
    });

    $(".custom-modal-close").on("click",function(){
        $(this).closest('.custom-modal').removeClass('custom-modal-show');
    });

    // display navbar-cart
    $(".navbar-nav .nav-link[data-target='navbar-cart']").mouseover(function(){
        $(this).parent(".nav-item").addClass('active').find(".navbar-cart").addClass("active");
    });
    $(".navbar-nav .nav-link[data-target='navbar-cart']").mouseout(function(){
        $(this).parent(".nav-item").removeClass('active').find(".navbar-cart").removeClass("active");
    });
    $(".navbar-nav .nav-item .navbar-cart").mouseover(function(e){
        e.stopPropagation();
        $(this).parent(".nav-item").addClass('active').find(".navbar-cart").addClass("active");
    });

    $('html').click(function(evt){      
        $(".nav-item").removeClass('active').find(".navbar-cart").removeClass("active");
    });
    $(".navbar-nav .nav-item .navbar-cart").on("click",function(e){
        e.stopPropagation();
    });
});

//geolocation
var x = document.getElementById("home-search");
function getLocation(){
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    }
    else{
        x.value="Geolocation is not supported by this browser.";
    }
}

function showPosition(position){
    lat=position.coords.latitude;
    lon=position.coords.longitude;
    displayLocation(lat,lon);
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            x.innerHTML="User denied the request for Geolocation."
        break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML="Location information is unavailable."
        break;
        case error.TIMEOUT:
            x.innerHTML="The request to get user location timed out."
        break;
        case error.UNKNOWN_ERROR:
            x.innerHTML="An unknown error occurred."
        break;
    }
}

function displayLocation(latitude,longitude){
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);

    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");

                    count=value.length;
                    country=value[count-1];
                    state=value[count-2];
                    city=value[count-3];
                    x.value = city;
                }
                else  {
                    x.value = "address not found";
                }
            }
            else {
                x.value = "Geocoder failed due to: " + status;
            }
        }
    );
}
 